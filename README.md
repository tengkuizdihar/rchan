# RChan

A feature-less 4Chan clone, made out of spite because the creator truly *hates* PHP.

# Documentations

For now the README is the only documentations that's available for most of the things in this project.

# Objective

This project has the objective to prove people that Rust could be used within a production setting.
The feature that shall be implemented is:

- [x] REST API kind of communication
- [x] Create a way to test the application through the HTTP Request
- [ ] Content creation
  - [ ] Post
    - [ ] Images
    - [ ] Text
    - [ ] Admin posts
    - [ ] Scheduled janitor work
  - [ ] Categories
    - [ ] Admin controlled and made
- [ ] Administration
  - [ ] Blacklisting IP range
  - [ ] Curating posts
  - [ ] Permissions
- [ ] Binary Files
  - [x] Process multipart form neatly
  - [x] Extract files from multipart form
  - [x] Storing and retrieving
    - [x] Local filesystem
    - [ ] S3
- [x] Generate PDF from an HTML
- [ ] Calling other API and handling the errors
- [ ] Scheduled post clean up
- [x] Stateless input validation
- [x] Auth
  - [x] Authorization
  - [x] Authentication (using JWT)
- [ ] Containerization
  - [ ] Docker based on NixOS
  - [ ] Docker compose
  - [ ] Kubernetes
- [ ] Gitlab CI/CD

# QnA

## Will this project be federated?

This project will not be federated. The reason is, this imageboard is not meant to hold data for a very long time and posts will eventually be deleted, to keep things fresh.

## Could you create categories as a user?

Unfortunately not, this will prevent spams and headache to moderators. But the community could definitely create a post and took the attention of the moderators to create a new categories though.

## Who will maintain this project?

This project will be maintained by me. Other people that's interested in this project should contact me and I will probably make them an admin of this repository if they are to be trusted in such matters. Alternatives that's forking this project should also contact me, I will consider to put your project's name in the top of the README.