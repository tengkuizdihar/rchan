import type { AxiosError } from "axios"
import { isObject, range, map, keys, get, isArray, isEmpty } from "lodash"

export type TerminalValues = undefined | null | string | number | boolean

export interface Map<T> {
  [k: string]: T
}

export interface Pagination {
  total: number
  page: number
}

export interface Payload<T> {
  data: T[]
  pagination: Pagination
}

export interface Response<T> {
  message: string
  payload: T
}

export interface ResponseList<T> {
  message: string
  payload: Payload<T>
}

export interface ErrorResponse {
  message: string
}

export type Nil = undefined | null
export type SuccessCallback<T, Y = void> = (data: Response<T>) => Y
export type ErrorCallback<T = ErrorResponse | string> = (
  data: AxiosError<T>
) => any

/**
 * Generate a range of number that could be used to render page number for pagination component.
 *
 * @param currentPage   The current page number
 * @param lastPage      The last page number
 * @param padding       The page number "padding" around the current page rendered
 * @returns             An array of numbers that could be used for rendering pagination number.
 */
export function getPageRange(
  currentPage: number,
  lastPage: number,
  padding: number
): number[] {
  // Page numbers padding left and right of the current page
  const lowerPadding = currentPage - padding
  const upperPadding = currentPage + padding

  // Create the default bound
  let lowerBound = Math.max(1, lowerPadding)
  let upperBound = Math.min(lastPage, upperPadding)

  // If current page nears first page, lengthen the padding on the right
  if (lowerPadding < 1) {
    upperBound = Math.min(lastPage, upperBound - lowerPadding + 1)
  }

  // If current page nears last page, lengthen the padding on the left
  if (upperPadding > lastPage) {
    lowerBound = Math.max(1, lowerBound - (upperPadding - lastPage))
  }

  // Return all indexes between lowerBound and upperBound. Lodash upper range is non-inclusive
  return range(lowerBound, upperBound + 1)
}

// Transform the given object into a format that is understandable by
// the library used in backend
//
// { first : { second : 2 } } => { "first[second]": 2 }
type KeyTypes = string | number
export function mapToNestedRequest<T>(m?: Map<T>): Map<TerminalValues> {
  let transformedMap: Map<TerminalValues> = {}
  let keysProcessingStack: KeyTypes[][] = map(keys(m), (v) => [v])

  // Recursively process this keys until it is on the base case which has the values of TerminalValues
  while (keysProcessingStack.length > 0) {
    let keyPath = keysProcessingStack.pop()!
    let value = get(m, keyPath)

    // Will process anything other than null or undefined
    if (isObject(value) || isArray(value)) {
      keys(value)
        .map((v) => [...keyPath, v])
        .forEach((v) => {
          keysProcessingStack.push(v)
        })
    } else {
      let stringKey = ""
      keyPath.forEach((key, i) => {
        if (i > 0) {
          stringKey += `[${key}]`
        } else {
          stringKey += key
        }
      })

      transformedMap[stringKey] = value
    }
  }

  return transformedMap
}
