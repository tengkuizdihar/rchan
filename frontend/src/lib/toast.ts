// @ts-ignore
import type { AxiosError } from "axios"
import { SvelteToastOptions, toast } from "@zerodevx/svelte-toast"

export function toastConfig(): SvelteToastOptions {
  return {
    duration: 20000,
    dismissable: true,
    initial: 0,
    progress: 0,
    reversed: false,
    intro: {},
    theme: {},
  }
}

export function toastError(
  message: string,
  error?: AxiosError,
  title?: string
) {
  title = title ?? "Error"

  let config = toastConfig()
  config.theme = {
    "--toastBackground": "#F56565",
    "--toastProgressBackground": "#C53030",
  }

  console.error(
    message,
    error?.message,
    error?.response?.data?.message,
    error?.stack
  )

  toast.push(`<strong>${title}</strong><br>${message}`, config)
}

export function toastSuccess(message: string) {
  toast.push(message)
}
