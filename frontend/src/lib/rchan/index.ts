import type { AxiosInstance, AxiosResponse } from "axios"
import type { Map, Response } from "$lib/utils"

import axios from "axios"
import { getContext } from "svelte"
import Cookies from "js-cookie"
import { Post } from "$lib/rchan/post"

export const COOKIE_TOKEN = "token"

export class User {
  api: AxiosInstance
  token?: string

  constructor(api: AxiosInstance, token?: string) {
    this.api = api
    this.token = token
  }
}

export class Rchan {
  baseUrl: string
  token?: string
  api: AxiosInstance

  constructor(url?: string, api?: AxiosInstance) {
    this.baseUrl = url || "http://localhost:8000"

    this.api =
      api ||
      axios.create({
        baseURL: this.baseUrl,
        headers: { Authorization: "Bearer " + this.token },
      })
  }

  setToken(token: string) {
    this.token = token
  }

  async login_anon() {
    let token = Cookies.get(COOKIE_TOKEN)

    if (token) {
      this.token = token
    } else {
      let res: AxiosResponse<Response<string>> = await this.api.post(
        "/users/login/anon"
      )

      this.token = res.data.payload
      Cookies.set(COOKIE_TOKEN, this.token!, { expires: 1 })
    }

    this.api = axios.create({
      baseURL: this.baseUrl,
      headers: { Authorization: "Bearer " + this.token },
    })
  }

  post() {
    return new Post(this.api)
  }

  user() {
    return new User(this.api, this.token)
  }

  getImage(imageName: string): string {
    return `${this.baseUrl}/documents/${imageName}`
  }
}

// Globally accessible rchan
export const rchanContext = "rchan"

export const getRchan = (): Rchan => {
  return getContext(rchanContext)
}

// Turn object to multipart
export const toFormData = (o: Map<any>): FormData => {
  let formData = new FormData()

  Object.entries(o).map(([k, v]) => {
    if (v != null) {
      if (v instanceof FileList) {
        for (let i = 0; i < v.length; i++) {
          formData.append(k, v[i])
        }
      } else {
        formData.append(k, v)
      }
    }
  })

  return formData
}
