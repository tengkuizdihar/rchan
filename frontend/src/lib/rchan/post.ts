import moment from "moment"
import type { AxiosInstance, AxiosResponse } from "axios"
import {
  Map,
  mapToNestedRequest,
  Response,
  ResponseList,
  TerminalValues,
} from "$lib/utils"
import { v4 as uuidv4 } from "uuid"
import { toFormData } from "."

// Key for the token owner local storage
// The type of the object in it is a map of post_id -> {token: string, created_at: moment}
const TOKEN_OWNER_KEY = "token_owner"

export interface TokenOwnerValue {
  // The token of a post used for deleting it
  token: string

  // ISO8601 string created from the library "moment"
  created_at: string
}

export interface PaginationParam {
  limit?: number
  page: number
}

export interface PostData {
  id: number
  title?: string
  content?: string
  parent_post_id?: number
  doc_path?: string
  created_at: string
  updated_at: string
}

export interface SearchPost extends PaginationParam, Map<TerminalValues> {
  title?: string
  content?: string
  parent_post_id?: number
  is_top_parent?: boolean
  category?: string
}

export class Post {
  api: AxiosInstance

  constructor(api: AxiosInstance) {
    this.api = api
  }

  async create<T>(
    form: Map<T>,
    category: string,
    parent_post_id?: number
  ): Promise<Response<PostData>> {
    let token_owner = uuidv4()
    let formData = toFormData({
      ...form,
      token_owner,
      parent_post_id,
      category,
    })

    let data: AxiosResponse<Response<PostData>> = await this.api.post(
      "/posts",
      formData,
      {
        headers: {
          "content-type": "multipart/form-data",
        },
      }
    )

    this.createTokenOwner(data.data.payload.id, token_owner)

    return data.data
  }

  async get(post_id: number): Promise<Response<PostData>> {
    let data = await this.api.get(`/posts/${post_id}`)
    return data.data
  }

  async list(params?: SearchPost): Promise<ResponseList<PostData>> {
    let data = await this.api.get("/posts", {
      params: mapToNestedRequest(params),
    })
    return data.data
  }

  async delete(post_id: number): Promise<Response<null>> {
    let token = this.getTokenOwner(post_id)?.token
    let data = await this.api.delete(`/posts/${token}`)

    this.deleteTokenOwner(post_id)

    return data.data
  }

  organizeTokenOwner() {
    let token_owner_map = JSON.parse(
      localStorage.getItem(TOKEN_OWNER_KEY) ?? "{}"
    )

    if (Object.keys(token_owner_map).length > 1000) {
      let entries: Array<[string, TokenOwnerValue]> =
        Object.entries(token_owner_map)

      // The latest will be in front
      entries = entries
        .sort(([_k1, v1], [_k2, v2]) =>
          moment(v2.created_at).diff(moment(v1.created_at))
        )
        .slice(0, 100)

      let latest_owner_map = Object.fromEntries(entries)
      localStorage.setItem(TOKEN_OWNER_KEY, JSON.stringify(latest_owner_map))
    }
  }

  createTokenOwner(post_id: number, token_owner: string) {
    let token_owner_map: Map<TokenOwnerValue> = JSON.parse(
      localStorage.getItem(TOKEN_OWNER_KEY) ?? "{}"
    )

    token_owner_map[String(post_id)] = {
      token: token_owner,
      created_at: moment().toISOString(),
    }

    localStorage.setItem(TOKEN_OWNER_KEY, JSON.stringify(token_owner_map))
    this.organizeTokenOwner()
  }

  getTokenOwner(post_id: number): TokenOwnerValue | undefined {
    let token_owner_map = JSON.parse(
      localStorage.getItem(TOKEN_OWNER_KEY) ?? "{}"
    )

    return token_owner_map[post_id]
  }

  deleteTokenOwner(post_id: number): void {
    let token_owner_map = JSON.parse(
      localStorage.getItem(TOKEN_OWNER_KEY) ?? "{}"
    )

    delete token_owner_map[post_id]

    localStorage.setItem(TOKEN_OWNER_KEY, JSON.stringify(token_owner_map))
    this.organizeTokenOwner()
  }
}
