# Setting Up The Application

To use the application, the developer/user need to follow the guides below without failing. Failing will result in unexpected behavior of the application.

## Prerequisite

- Installed the latest stable rust.
- Have a postgresql database running.
  - You can make another database for testing and could be seen the configuration in .env.example.
- Installed diesel via cargo or other binary distribution.
- Make sure you running linux or any other plebian operating system that uses Makefile. If you don't have one, you can infer the behavior of the makefile by reading `Makefile`.
- To be able to use the auto-reload feature, you must install cargo-watch by using `cargo install cargo-watch` command.
  - You could activate it using `make watch` command in the rust project directory.

## First Time Setup

1. To fill your currently used database denoted by the environment variable `DATABASE_URL`, one must use `diesel migration run` that would apply any pending migrations.

## Setup

1. create a new .env using .env.example
2. cargo run

## Testing Environment

The application that we're using to test HTTP is Insomnia.

After the first time setup is done, use `make test` to run the test in its entirety.
