let
  d = import ./dependencies.nix { };
  pkgs = d.pkgs;
in
pkgs.clangStdenv.mkDerivation {
  name = "clang-nix-shell";
  buildInputs = d.shellInputs ++ d.buildInputs;
  RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";

}
