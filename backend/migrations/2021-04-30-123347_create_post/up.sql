-- Your SQL goes here
CREATE TABLE post (
  id BIGSERIAL PRIMARY KEY,
  title text NULL,
  content text,
  category VARCHAR(200),
  token_owner VARCHAR(200) UNIQUE NOT NULL,
  parent_post_id BIGINT references post (id) ON DELETE CASCADE,
  doc_path VARCHAR(4096) NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE TRIGGER auto_updated_post
BEFORE UPDATE ON post
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();