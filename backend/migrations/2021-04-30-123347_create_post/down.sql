-- This file should undo anything in `up.sql`
DROP TRIGGER IF EXISTS auto_updated_post ON post;
DROP TABLE IF EXISTS post;