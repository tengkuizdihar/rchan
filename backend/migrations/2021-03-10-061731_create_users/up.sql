-- Why app_user and not user? Because postgresql reserved that name first.
-- Your SQL goes here
CREATE TABLE app_user (
  id BIGSERIAL PRIMARY KEY,
  email VARCHAR(60) UNIQUE NOT NULL,
  display_name VARCHAR(60) NOT NULL,
  role_id INTEGER NOT NULL,
  pass_hash VARCHAR(100) NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE TRIGGER auto_updated_app_user 
BEFORE UPDATE ON app_user 
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();
