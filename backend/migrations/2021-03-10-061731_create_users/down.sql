-- This file should undo anything in `up.sql`
-- NOTE: the dropping should be ordered in TRIGGER => FUNCTION => USERS
DROP TRIGGER IF EXISTS auto_updated_app_user ON app_user;
DROP TABLE IF EXISTS app_user;