{ pkgs ? import (fetchTarball ("https://github.com/NixOS/nixpkgs/archive/3e644bd62489b516292c816f70bf0052c693b3c7.tar.gz")) { } }:
{
  inherit pkgs;

  buildInputs = [
    pkgs.pkgconfig
    pkgs.postgresql
  ];

  shellInputs = [
    pkgs.rustc
    pkgs.cargo
    pkgs.clippy
    pkgs.rustfmt
    pkgs.diesel-cli
    pkgs.cargo-watch
    pkgs.bc
    pkgs.k6
  ];
}
