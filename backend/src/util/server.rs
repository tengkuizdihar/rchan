use crate::handler::errors::GeneralError;
use crate::service::ServiceRepo;
use crate::{repository::psql::DbPool, service::user::NewUserRaw};
use dotenv::dotenv;
use std::env::{set_var, var};

// Create a super user with some kinds of password coming from the environment variable
const ADMIN_EMAIL: &str = "admin@admin.com";
const ADMIN_USERNAME: &str = "admin";
const ADMIN_ROLE: i32 = 1;

pub fn establish_super_admin(pool: DbPool) {
    let conn = pool.get().expect("Postgres connection doesn't exist");
    let sr = ServiceRepo::new(conn);

    if let Err(GeneralError::Database(diesel::result::Error::NotFound)) =
        sr.s.user.index_by_subject(&sr, ADMIN_EMAIL)
    {
        let new_user_form = NewUserRaw {
            email: ADMIN_EMAIL.to_string(),
            display_name: ADMIN_USERNAME.to_string(),
            role_id: ADMIN_ROLE,
            password: var("SUPERADMIN_PASSWORD")
                .expect("Need SUPERADMIN_PASSWORD in the environment variable"),
        };

        let _ =
            sr.s.user
                .create_admin_internal(&sr, new_user_form)
                .expect("Failed creating super user, how.");
    }
}

#[derive(Debug, Clone)]
pub enum ApplicationPreset {
    Production,
    Debug,
}

#[derive(Clone, Debug)]
pub struct ApplicationConfiguration {
    /// Example would be "127.0.0.1:8080"
    pub socket_address: String,

    /// The application deployment preset
    pub preset: ApplicationPreset,

    /// Secret key
    pub secret_key: String,
}

impl ApplicationConfiguration {
    pub fn new() -> Self {
        dotenv().ok();

        // Creating the config
        set_var("RUST_BACKTRACE", "1");
        let socket_address = var("SOCKET_ADDRESS").unwrap_or_else(|_| "localhost:8000".to_string());
        let preset_env = var("APPLICATION_PRESET").unwrap_or_else(|_| "DEBUG".to_string());
        let secret_key = var("SECRET_KEY").unwrap();
        let preset = match preset_env.as_str() {
            "PRODUCTION" => ApplicationPreset::Production,
            _ => ApplicationPreset::Debug,
        };

        // Check environment variable and panic when not available
        let _ = var("RUST_LOG").expect("Environment Variable RUST_LOG is not set");
        env_logger::init();

        ApplicationConfiguration {
            socket_address,
            preset,
            secret_key,
        }
    }
}
