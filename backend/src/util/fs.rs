use crate::handler::data::GeneralResult;
use crate::handler::errors::{ErrorFault, GeneralError};

use awmp::File;
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use std::fs::remove_file;
use std::path::{Path, PathBuf};

pub const LOCAL_STORAGE_TEST_LOCATION: &str = "./storage-test";

pub struct LocalFS;

impl LocalFS {
    pub fn checked_random_filename(original_filename: &str) -> GeneralResult<String> {
        // Prepare the file for storage
        let mut random_filename = Self::create_random_filename(original_filename);

        while Self::exists(random_filename.as_str())? {
            random_filename = Self::create_random_filename(original_filename);
        }

        // Create the path of the file
        Ok(random_filename)
    }

    fn create_random_filename(original_filename: &str) -> String {
        let mut rng = thread_rng();
        let name: String = (&mut rng)
            .sample_iter(Alphanumeric)
            .take(32)
            .map(char::from)
            .collect();

        format!("{}-{}", name, original_filename)
    }

    pub fn delete<P>(file_path: P) -> GeneralResult<()>
    where
        P: AsRef<Path>,
    {
        let storage_location = Self::get_storage_location();
        let path = [storage_location.as_ref(), file_path.as_ref()]
            .iter()
            .collect::<PathBuf>();

        match remove_file(path) {
            Ok(_) => Ok(()),
            Err(_) => Err(GeneralError::File(
                "Failed deleting file in temporary".to_string(),
                ErrorFault::Server,
            )),
        }
    }

    pub fn exists<P>(file_path: P) -> GeneralResult<bool>
    where
        P: AsRef<Path>,
    {
        let storage_path = Self::get_storage_location().join(file_path);

        Ok(storage_path.exists())
    }

    pub fn get_storage_location() -> PathBuf {
        if cfg!(test) {
            PathBuf::from(LOCAL_STORAGE_TEST_LOCATION)
        } else {
            let storage_location =
                std::env::var("LOCAL_STORAGE_LOCATION").unwrap_or_else(|_| "./storage".to_string());

            PathBuf::from(storage_location)
        }
    }

    /// Save a single file inside of a storage with random name assigned to it
    pub fn save<P>(path: P, file: File) -> GeneralResult<std::fs::File>
    where
        P: AsRef<Path>,
    {
        let storage_location = Self::get_storage_location().join(path);
        Ok(file.persist_at(storage_location)?)
    }
}

#[cfg(test)]
#[allow(dead_code)]
impl LocalFS {
    pub fn create_fake_file(original_file_name: String, size: usize) -> awmp::File {
        // Digit one in UTF-8 according to https://www.utf8-chartable.de/
        let test_letter = 0x31;

        // Create the test content
        let content = std::iter::repeat(test_letter)
            .take(size)
            .collect::<Vec<u8>>();

        // Create the temporary file and fill it with the test content
        let mut temp = awmp::NamedTempFile::new_in(LOCAL_STORAGE_TEST_LOCATION)
            .expect("Cannot create temp file in local storage location");

        std::io::Write::write_all(&mut temp, &content)
            .expect("Failed writing fakes to temporary file");

        // Give them the file
        awmp::File::new_with_file_name(temp, original_file_name)
    }

    pub fn refresh_local_test_storage() {
        if std::path::Path::new(LOCAL_STORAGE_TEST_LOCATION).exists() {
            std::fs::remove_dir_all(LOCAL_STORAGE_TEST_LOCATION)
                .expect("Can't remove directory for testing storage");
        }

        std::fs::create_dir(LOCAL_STORAGE_TEST_LOCATION)
            .expect("Can't create directory for testing storage");
    }
}
