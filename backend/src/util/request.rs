use actix::fut::{err, ok, Ready};
use actix_governor::{Governor, GovernorConfigBuilder, PeerIpKeyExtractor};
use actix_web::FromRequest;
use serde::de;
use serde_qs::Config;
use std::{fmt, ops, time::Duration};

use crate::handler::{data::GeneralResult, errors::GeneralError};

pub fn rate_limiter() -> Governor<PeerIpKeyExtractor> {
    let config = GovernorConfigBuilder::default()
        .burst_size(10)
        .period(Duration::from_millis(100))
        .finish()
        .expect("Governor Configuration successfully built");

    Governor::new(&config)
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
pub struct NestedQuery<T>(pub T);

impl<T> NestedQuery<T> {
    pub fn into_inner(self) -> T {
        self.0
    }

    /// Get query parameters from the path
    pub fn from_query<S>(query_str: S) -> GeneralResult<T>
    where
        T: de::DeserializeOwned,
        S: AsRef<str>,
    {
        Config::new(5, true)
            .deserialize_str(query_str.as_ref())
            .map_err(GeneralError::QueryStringParse)
    }
}

impl<T> FromRequest for NestedQuery<T>
where
    T: de::DeserializeOwned,
{
    type Error = GeneralError;

    type Future = Ready<GeneralResult<Self>>;

    #[inline]
    fn from_request(req: &actix_web::HttpRequest, _: &mut actix_web::dev::Payload) -> Self::Future {
        let query = match urlencoding::decode(req.query_string()) {
            Ok(a) => a,
            Err(e) => return err(GeneralError::QueryStringParse(serde_qs::Error::FromUtf8(e))),
        };

        let parsed = match Self::from_query(query) {
            Ok(p) => p,
            Err(e) => return err(e),
        };

        ok(Self(parsed))
    }
}

impl<T> ops::Deref for NestedQuery<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

impl<T> ops::DerefMut for NestedQuery<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.0
    }
}

impl<T: fmt::Debug> fmt::Debug for NestedQuery<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl<T: fmt::Display> fmt::Display for NestedQuery<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

#[cfg(test)]
mod test {
    use crate::{util::request::NestedQuery, prelude::*};
    use actix_web::{test::TestRequest, FromRequest};

    #[derive(Clone, Deserialize, Debug)]
    pub struct RequestFlat {
        pub number: Option<i32>,
        pub text: Option<String>,
    }

    #[derive(Clone, Deserialize, Debug)]
    pub struct RequestNested {
        pub text: Option<String>,
        pub nested: Option<RequestFlat>,
    }

    #[actix_rt::test]
    async fn ok_request_flat() {
        let params = vec!["number=1", "text=ayylmao"].join("&");
        let request_string = format!("/?{}", params);

        let (req, mut pl) = TestRequest::with_uri(&request_string)
            .to_srv_request()
            .into_parts();

        let parsed = NestedQuery::<RequestFlat>::from_request(&req, &mut pl)
            .await
            .unwrap();

        assert_eq!(Some(1), parsed.number);
        assert_eq!(Some("ayylmao".to_string()), parsed.text);
    }

    #[actix_rt::test]
    async fn ok_request_nested() {
        let params = vec![
            "nested[number]=1",
            "nested[text]=nested-text",
            "text=text-outside",
        ]
        .join("&");

        let request_string = format!("/?{}", params);

        let (req, mut pl) = TestRequest::with_uri(&request_string)
            .to_srv_request()
            .into_parts();

        let parsed = NestedQuery::<RequestNested>::from_request(&req, &mut pl)
            .await
            .unwrap();

        assert_eq!(1, parsed.clone().nested.unwrap().number.unwrap());
        assert_eq!(
            "nested-text".to_string(),
            parsed.clone().nested.unwrap().text.unwrap()
        );
        assert_eq!("text-outside".to_string(), parsed.clone().text.unwrap());
    }
}
