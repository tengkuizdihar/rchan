use chrono::{Duration, Utc};
use jsonwebtoken::{encode, EncodingKey};
use pbkdf2::password_hash::{PasswordHash, PasswordHasher, PasswordVerifier, SaltString};
use pbkdf2::Pbkdf2;
use rand_core::OsRng;

use crate::handler::auth::{Claims, JwtRequest};
use crate::handler::data::{GeneralResult, PagedResponse, PaginationRequest};
use crate::handler::errors::GeneralError;
use crate::prelude::*;
use crate::repository::psql::prelude::*;
use crate::repository::psql::schema::app_user;
use crate::repository::psql::AuthExpression;
use crate::service::ServiceRepo;

pub const JWT_ISSUER: &str = "rchan";
pub const JWT_ANON_SUBJECT: &str = "anon";

#[cfg_attr(test, automock)]

pub trait UserService {
    fn index(&self, sr: &ServiceRepo, jwt: &JwtRequest, pk: i64) -> GeneralResult<User>;

    fn delete(&self, sr: &ServiceRepo, jwt: &JwtRequest, pk: i64) -> GeneralResult<()>;

    fn create(&self, sr: &ServiceRepo, jwt: &JwtRequest, model: NewUserRaw) -> GeneralResult<User>;

    fn update(
        &self,
        sr: &ServiceRepo,
        jwt: &JwtRequest,
        model: UpdateUserRaw,
        pk: i64,
    ) -> GeneralResult<User>;

    fn search(
        &self,
        sr: &ServiceRepo,
        jwt: &JwtRequest,
        model: &SearchUser,
        pagination: &PaginationRequest,
    ) -> GeneralResult<PagedResponse<User>>;

    fn authorize<'a>(
        &self,
        user: &User,
        action: UserAction<'a>,
    ) -> GeneralResult<AuthExpression<app_user::table>>;

    /// WARNING: ONLY USE THIS FOR A VERY SPECIFIC AND WELL THOUGHT OUT PROCESSES WHERE USER INPUT FROM DEVELOPER ONLY!
    fn create_admin_internal(&self, sr: &ServiceRepo, model: NewUserRaw) -> GeneralResult<User>;

    fn index_by_subject(&self, sr: &ServiceRepo, subject: &str) -> GeneralResult<User>;

    fn login(&self, sr: &ServiceRepo, form: LoginNative, secret: &str) -> GeneralResult<String>;

    fn login_anon(&self, secret: &str) -> GeneralResult<String>;

    // Display the information for the user
    fn whoami(&self, sr: &ServiceRepo, jwt: &JwtRequest) -> GeneralResult<User>;
}

pub struct UserServiceImpl;

#[derive(Debug, Deserialize, Serialize, Queryable, Clone, PartialEq, Eq)]
pub struct User {
    pub id: i64,
    pub email: String,
    pub display_name: String,
    pub role_id: i32,

    #[serde(skip_serializing)]
    pub pass_hash: String,

    pub created_at: chrono::NaiveDateTime,
    pub updated_at: chrono::NaiveDateTime,
}

#[derive(Debug, Deserialize, Serialize, Validate)]
pub struct NewUserRaw {
    #[validate(email)]
    pub email: String,

    #[validate(length(min = 5))]
    pub display_name: String,

    pub role_id: i32,
    pub password: String,
}

#[derive(Debug, Clone, Deserialize, Serialize, Validate)]
pub struct UpdateUserRaw {
    #[validate(length(min = 5))]
    pub display_name: Option<String>,
    pub role_id: Option<i32>,
    pub password: Option<String>,
}

#[derive(Debug, Deserialize, Serialize, Insertable)]
#[table_name = "app_user"]
pub struct NewUser {
    pub email: String,
    pub display_name: String,
    pub role_id: i32,
    pub pass_hash: String,
}

#[derive(Debug, Deserialize, Serialize, AsChangeset, Validate)]
#[table_name = "app_user"]
pub struct UpdateUser {
    #[validate(length(min = 5))]
    pub display_name: Option<String>,

    pub role_id: Option<i32>,
    pub pass_hash: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct SearchUser {
    pub id: Option<i64>,
    pub email: Option<String>,
    pub display_name: Option<String>,
    pub role_id: Option<i32>,
}

#[derive(Debug, Deserialize)]
pub struct LoginNative {
    pub email: String,
    pub password: String,
}

#[derive(Debug)]
pub enum UserAction<'a> {
    Create,
    Read,
    Delete,
    Update(&'a UpdateUser),
}

#[derive(Debug, FromPrimitive, ToPrimitive)]
pub enum Role {
    Anonymous = 0,
    Admin = 1,
    Moderators = 2,
}

impl UserService for UserServiceImpl {
    fn index(&self, sr: &ServiceRepo, jwt: &JwtRequest, pk: i64) -> GeneralResult<User> {
        let user = sr.s.user.index_by_subject(sr, &jwt.sub)?;
        sr.r.user.get_by_id_auth(sr, &user, pk)
    }

    fn delete(&self, sr: &ServiceRepo, jwt: &JwtRequest, pk: i64) -> GeneralResult<()> {
        let user = sr.s.user.index_by_subject(sr, &jwt.sub)?;
        sr.r.user.delete_by_id_auth(sr, &user, pk)?;
        Ok(())
    }

    fn create(&self, sr: &ServiceRepo, jwt: &JwtRequest, model: NewUserRaw) -> GeneralResult<User> {
        let salt = SaltString::generate(&mut OsRng);
        let user = sr.s.user.index_by_subject(sr, &jwt.sub)?;
        let _ = sr.s.user.authorize(&user, UserAction::Create)?;

        match salted_hash(model.password.as_str(), &salt) {
            Ok(p) => {
                let new_model = NewUser {
                    email: model.email,
                    display_name: model.display_name,
                    role_id: model.role_id,
                    pass_hash: p.to_string(),
                };

                Ok(sr.r.user.create(sr, new_model)?)
            }
            Err(e) => {
                log::error!("Password hashing error! {:?}", e);
                Err(GeneralError::Authentication)
            }
        }
    }

    fn update(
        &self,
        sr: &ServiceRepo,
        jwt: &JwtRequest,
        model: UpdateUserRaw,
        pk: i64,
    ) -> GeneralResult<User> {
        let user = sr.s.user.index_by_subject(sr, &jwt.sub)?;
        let salt = SaltString::generate(&mut OsRng);
        let mut new_model = UpdateUser {
            display_name: model.display_name,
            role_id: model.role_id,
            pass_hash: None,
        };

        if let Some(password) = model.password {
            match salted_hash(password.as_str(), &salt) {
                Ok(p) => new_model.pass_hash = Some(p.to_string()),
                Err(e) => {
                    log::error!("Password hashing error! {:?}", e);
                    return Err(diesel::result::Error::RollbackTransaction.into());
                }
            };
        }

        sr.r.user.update_user_by_id_auth(sr, &user, pk, &new_model)
    }

    fn search(
        &self,
        sr: &ServiceRepo,
        jwt: &JwtRequest,
        model: &SearchUser,
        pagination: &PaginationRequest,
    ) -> GeneralResult<PagedResponse<User>> {
        let user = sr.s.user.index_by_subject(sr, &jwt.sub)?;

        let (data, total, page) = sr.r.user.search_auth(sr, &user, model, pagination)?;

        Ok(PagedResponse::new(data, total, page))
    }

    fn authorize(
        &self,
        user: &User,
        action: UserAction,
    ) -> GeneralResult<AuthExpression<app_user::table>> {
        let role: Role = FromPrimitive::from_i32(user.role_id).to_general_error()?;

        match action {
            // Only admin can create user
            UserAction::Create => match role {
                Role::Admin => Ok(Box::new(app_user::id.is_not_null())),
                _ => Err(GeneralError::Authorization(
                    "Only admin can create user.".to_string(),
                )),
            },

            // Only admin can read all of the people's data
            UserAction::Read | UserAction::Delete => match role {
                Role::Admin => Ok(Box::new(app_user::id.is_not_null())),
                Role::Anonymous => Err(GeneralError::Authorization(
                    "Please log in to continue".to_string(),
                )),
                _ => Ok(Box::new(app_user::id.eq(user.id))),
            },

            // Only admin can update the roles of app_user
            UserAction::Update(u) => match u.role_id {
                Some(_) => match role {
                    Role::Admin => Ok(Box::new(app_user::id.is_not_null())),
                    _ => Err(GeneralError::Authorization(
                        "Only admin are able to change roles.".to_string(),
                    )),
                },
                None => match role {
                    Role::Admin => Ok(Box::new(app_user::id.is_not_null())),
                    Role::Anonymous => Err(GeneralError::Authorization(
                        "Please log in to continue".to_string(),
                    )),
                    _ => Ok(Box::new(app_user::id.eq(user.id))),
                },
            },
        }
    }

    /// WARNING: ONLY USE THIS FOR A VERY SPECIFIC AND WELL THOUGHT OUT PROCESSES WHERE USER INPUT FROM DEVELOPER ONLY!
    fn create_admin_internal(&self, sr: &ServiceRepo, model: NewUserRaw) -> GeneralResult<User> {
        let salt = SaltString::generate(&mut OsRng);

        match salted_hash(model.password.as_str(), &salt) {
            Ok(p) => {
                let new_model = NewUser {
                    email: model.email,
                    display_name: model.display_name,
                    role_id: model.role_id,
                    pass_hash: p.to_string(),
                };

                Ok(sr.r.user.create(sr, new_model)?)
            }
            Err(e) => {
                log::error!("Password hashing error! {:?}", e);
                Err(GeneralError::Authentication)
            }
        }
    }

    fn index_by_subject(&self, sr: &ServiceRepo, subject: &str) -> GeneralResult<User> {
        match subject {
            JWT_ANON_SUBJECT => Ok(create_anon_user()),
            _ => sr.r.user.get_by_email(sr, subject),
        }
    }

    fn login(&self, sr: &ServiceRepo, form: LoginNative, secret: &str) -> GeneralResult<String> {
        let user: User = sr.r.user.get_by_email(sr, &form.email)?;

        // Prepare the password hash from database
        let hashed_pass = match PasswordHash::new(user.pass_hash.as_ref()) {
            Ok(a) => a,
            Err(_) => {
                log::error!(
                    "Somehow pass_hash from database aren't working correctly for user {}",
                    user.email
                );

                return Err(GeneralError::PasswordHashing);
            }
        };

        let plain_pass = form.password.as_str();
        match Pbkdf2.verify_password(plain_pass.as_bytes(), &hashed_pass) {
            Ok(_) => Ok(create_jwt(&user, secret)?),
            Err(_) => Err(GeneralError::Authentication),
        }
    }

    fn login_anon(&self, secret: &str) -> GeneralResult<String> {
        // Prepare the password hash from database
        create_anon_jwt(secret)
    }

    // Display the information for the user
    fn whoami(&self, sr: &ServiceRepo, jwt: &JwtRequest) -> GeneralResult<User> {
        sr.s.user.index_by_subject(sr, jwt.sub.as_ref())
    }
}

fn create_anon_jwt(secret: &str) -> Result<String, GeneralError> {
    // Set the expired time to be one year from now
    let expired_time = Utc::now() + Duration::weeks(53);
    let expired_time = expired_time.timestamp();

    let claims = Claims {
        exp: expired_time,
        iss: JWT_ISSUER.to_string(),
        sub: JWT_ANON_SUBJECT.to_string(),
    };

    encode(
        &Default::default(),
        &claims,
        &EncodingKey::from_secret(secret.as_bytes()),
    )
    .map_err(|_| GeneralError::Authentication)
}

fn create_anon_user() -> User {
    let role_id = ToPrimitive::to_i32(&Role::Anonymous)
        .to_general_error()
        .expect("Anonymous role is somehow overflow the i32:MIN and i32::MAX");

    User {
        id: 0,
        email: "invalid".to_string(),
        display_name: "anon".to_string(),
        pass_hash: "invalid".to_string(),
        created_at: Utc::now().naive_utc(),
        updated_at: Utc::now().naive_utc(),
        role_id,
    }
}

fn create_jwt(user: &User, secret: &str) -> Result<String, GeneralError> {
    // Set the expired time to be one year from now
    let expired_time = Utc::now() + Duration::weeks(53);
    let expired_time = expired_time.timestamp();

    let claims = Claims {
        exp: expired_time,
        iss: JWT_ISSUER.to_string(),
        sub: user.email.clone(),
    };

    encode(
        &Default::default(),
        &claims,
        &EncodingKey::from_secret(secret.as_bytes()),
    )
    .map_err(|_| GeneralError::Authentication)
}

/// Should be used for creating password hash to be stored in the database
pub fn salted_hash<'a, S>(plaintext: &'a str, salt: &'a S) -> Result<PasswordHash<'a>, GeneralError>
where
    S: AsRef<str>,
{
    let password = plaintext.as_bytes();

    match Pbkdf2.hash_password_simple(password, salt) {
        Ok(p) => Ok(p),
        Err(e) => {
            log::error!("Password hashing error! {:?}", e);
            Err(GeneralError::PasswordHashing)
        }
    }
}

#[cfg(test)]
mod user_service_impl_test {
    use crate::repository::psql::prelude::*;
    use crate::repository::psql::schema::app_user;
    use crate::repository::psql::test::create_test;
    use crate::repository::psql::user::MockUserRepository;
    use crate::service::user::{NewUserRaw, Role, UpdateUserRaw};
    use mockall::predicate::*;

    use super::*;

    #[test]
    fn ok_create_new_user() {
        // ARRANGE
        let (_, _, jwt) = create_test().expect("Cannot create new admin");

        let mock_model = NewUserRaw {
            email: "testuser@gmail.com".to_string(),
            display_name: "testuser".to_string(),
            role_id: Role::Admin as i32,
            password: "123456".to_string(),
        };

        let naive_date_now = Utc::now().naive_utc();
        let mock_user = User {
            id: 1,
            email: "testuser@gmail.com".to_string(),
            display_name: "testuser".to_string(),
            role_id: Role::Admin as i32,
            pass_hash: "inihashpassword".to_string(),
            created_at: naive_date_now,
            updated_at: naive_date_now,
        };

        // ARRANGE - MOCK USER SERVICE
        let mut user_service = MockUserService::new();

        let user_result = mock_user.clone();
        user_service
            .expect_index_by_subject()
            .with(always(), always())
            .return_once(move |_, _| Ok(user_result));

        user_service
            .expect_authorize()
            .with(always(), always())
            .return_once(|_, _| Ok(Box::new(app_user::id.is_not_null())));

        // ARRANGE - MOCK USER REPOSITORY
        let mut user_repository = MockUserRepository::new();

        let user_result = mock_user.clone();
        user_repository
            .expect_create()
            .with(always(), always())
            .return_once(|_, _| Ok(user_result));

        // ARRANGE - FINALIZE MOCK
        let mut sr = ServiceRepo::mock();
        sr.s.user = Box::new(user_service);
        sr.r.user = Box::new(user_repository);

        // ACT
        let result = UserServiceImpl
            .create(&sr, &jwt, mock_model)
            .expect("Cannot create a user");

        // ASSERT
        assert_eq!(&result, &mock_user);
    }

    #[test]
    fn ok_create_new_user_twice() {
        // ARRANGE
        let (_, _, jwt) = create_test().expect("Cannot create new admin");

        let mock_model = NewUserRaw {
            email: "testuser@gmail.com".to_string(),
            display_name: "testuser".to_string(),
            role_id: Role::Admin as i32,
            password: "123456".to_string(),
        };

        let naive_date_now = Utc::now().naive_utc();
        let mock_user = User {
            id: 1,
            email: "testuser@gmail.com".to_string(),
            display_name: "testuser".to_string(),
            role_id: Role::Admin as i32,
            pass_hash: "inihashpassword".to_string(),
            created_at: naive_date_now,
            updated_at: naive_date_now,
        };

        // ARRANGE - MOCK USER SERVICE
        let mut user_service = MockUserService::new();

        let user_result = mock_user.clone();
        user_service
            .expect_index_by_subject()
            .with(always(), always())
            .return_once(move |_, _| Ok(user_result));

        user_service
            .expect_authorize()
            .with(always(), always())
            .return_once(|_, _| Ok(Box::new(app_user::id.is_not_null())));

        // ARRANGE - MOCK USER REPOSITORY
        let mut user_repository = MockUserRepository::new();

        let user_result = mock_user.clone();
        user_repository
            .expect_create()
            .with(always(), always())
            .return_once(|_, _| Ok(user_result));

        // ARRANGE - FINALIZE MOCK
        let mut sr = ServiceRepo::mock();
        sr.s.user = Box::new(user_service);
        sr.r.user = Box::new(user_repository);

        // ACT
        let result = UserServiceImpl
            .create(&sr, &jwt, mock_model)
            .expect("Cannot create a user");

        // ASSERT
        assert_eq!(&result, &mock_user);
    }

    #[test]
    fn ok_delete_user() {
        // ARRANGE
        let (_, _, jwt) = create_test().expect("Cannot create new admin");

        let naive_date_now = Utc::now().naive_utc();
        let mock_user = User {
            id: 1,
            email: "testuser@gmail.com".to_string(),
            display_name: "testuser".to_string(),
            role_id: Role::Admin as i32,
            pass_hash: "inihashpassword".to_string(),
            created_at: naive_date_now,
            updated_at: naive_date_now,
        };

        // ARRANGE - MOCK USER SERVICE
        let mut user_service = MockUserService::new();
        user_service
            .expect_index_by_subject()
            .with(always(), always())
            .return_once(|_, _| Ok(mock_user));

        // ARRANGE - MOCK USER REPOSITORY
        let mut user_repository = MockUserRepository::new();
        user_repository
            .expect_delete_by_id_auth()
            .with(always(), always(), eq(1))
            .return_once(|_, _, _| Ok(()));

        // ARRANGE - FINALIZE MOCK
        let mut sr = ServiceRepo::mock();
        sr.s.user = Box::new(user_service);
        sr.r.user = Box::new(user_repository);

        // ACT - SUCCESS IF NO ERROR
        let _ = UserServiceImpl
            .delete(&sr, &jwt, 1)
            .expect("Cannot create a user");
    }

    #[test]
    fn ok_update_user() {
        // ARRANGE
        let (_, _, jwt) = create_test().expect("Cannot create new admin");

        let update_model = UpdateUserRaw {
            display_name: Some("updateduser".to_string()),
            role_id: ToPrimitive::to_i32(&Role::Moderators),
            password: None,
        };

        let naive_date_now = Utc::now().naive_utc();
        let mock_user = User {
            id: 1,
            email: "testuser@gmail.com".to_string(),
            display_name: "testuser".to_string(),
            role_id: Role::Admin as i32,
            pass_hash: "inihashpassword".to_string(),
            created_at: naive_date_now,
            updated_at: naive_date_now,
        };

        // ARRANGE - MOCK USER SERVICE
        let mut user_service = MockUserService::new();
        user_service
            .expect_index_by_subject()
            .with(always(), always())
            .return_once(|_, _| Ok(mock_user));

        // ARRANGE - MOCK USER REPOSITORY
        let mut user_repository = MockUserRepository::new();
        user_repository
            .expect_update_user_by_id_auth()
            .with(always(), always(), eq(1), always())
            .return_once(move |_, _, _, _| {
                Ok(User {
                    id: 1,
                    email: "testuser@gmail.com".to_string(),
                    display_name: "updateduser".to_string(),
                    role_id: Role::Moderators as i32,
                    pass_hash: "inihashpassword".to_string(),
                    created_at: naive_date_now,
                    updated_at: naive_date_now,
                })
            });

        // ARRANGE - FINISHING MOCKING
        let mut sr = ServiceRepo::mock();
        sr.s.user = Box::new(user_service);
        sr.r.user = Box::new(user_repository);

        // ACT
        let updated_user = UserServiceImpl
            .update(&sr, &jwt, update_model.clone(), 1)
            .expect("Cannot update user");

        // ASSERT
        assert_eq!(
            &updated_user.display_name,
            &update_model.display_name.unwrap()
        );
        assert_eq!(&updated_user.role_id, &update_model.role_id.unwrap());
    }
}
