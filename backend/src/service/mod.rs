use crate::{
    mock::Realness,
    repository::{psql::DbConn, Repository},
    service::{post::PostServiceImpl, user::UserServiceImpl},
};

pub mod post;
pub mod user;

pub struct Services {
    pub user: Box<dyn user::UserService>,
    pub post: Box<dyn post::PostService>,
}

pub struct ServiceRepo {
    pub s: Services,
    pub r: Repository,
    pub config: Config,
}

pub struct Config {
    conn: Realness<DbConn>,
}

impl Services {
    pub fn new() -> Self {
        Self {
            user: Box::new(UserServiceImpl),
            post: Box::new(PostServiceImpl),
        }
    }

    #[cfg(test)]
    pub fn mock() -> Services {
        Self {
            user: Box::new(crate::service::user::MockUserService::new()),
            post: Box::new(crate::service::post::MockPostService::new()),
        }
    }
}

impl ServiceRepo {
    pub fn new(db_conn: DbConn) -> Self {
        Self {
            s: Services::new(),
            r: Repository::new(),
            config: Config::new(db_conn),
        }
    }

    #[cfg(test)]
    pub fn mock() -> Self {
        Self {
            s: Services::mock(),
            r: Repository::mock(),
            config: Config::mock(),
        }
    }
}
impl Config {
    fn new(conn: DbConn) -> Config {
        Config {
            conn: Realness::Real(conn),
        }
    }

    #[cfg(test)]
    fn mock() -> Config {
        Config {
            conn: Realness::Mock,
        }
    }

    /// Get a reference to the config's conn.
    pub fn conn(&self) -> &DbConn {
        self.conn.as_ref()
    }
}
