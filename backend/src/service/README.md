# Model Conventions
Most model in the system would have CRUD operation in them. Those operations can be done using diesel's ORM and impl methods for certain structs. Below are the description for each one. These examples could be seen in `src/database/user.rs`.

## Structs Format for A Single Table
The standard used in the project is by using a single Queryable (trait) struct as representation of a single table. This struct must use a trait called CommonModel and implement it themselves. The developer could also add more methods to the Queryable model as needed.

## Testing Using PostgreSQL
In this project, testing is done using the most defensive way possible, which is by *connecting to a test database and migrating and revert the migration for every single test*. This of course have some drawbacks and a VERY very important benefits if compared with the alternatives.

The benefits of using this method is:
1. **It's consistent** with the main database choice used. Using in memory SQLite might seem to be a good idea, but there's a lot of differences that can't be negotiated with such as the small differences in syntax and choice of terminologies in the `up.sql` and `down.sql`.

The drawbacks:
1. **It's slow** as of right now, it's very slow because migration and reverting the migration must be done for *every single test*. This could be fixed by implementing the test_transaction for every single test but there must be a function that could be called before a single module of test has begun for example.
2. **It requires more memory** to test. Let's admit it, using in memory SQLite instance is a lot cheaper than maintaining a single postgresql instance with multiple databases. It's admittedly debatable, but I have a bit of confidence that this is the case.