use std::str::FromStr;

use awmp::File;
use diesel::result::Error;

use crate::handler::data::{GeneralResult, PagedResponse, PaginationRequest};
use crate::handler::errors::GeneralError;
use crate::prelude::*;
use crate::repository::psql::prelude::*;
use crate::repository::psql::schema::post;
use crate::service::ServiceRepo;
use crate::util::fs::LocalFS;

#[cfg_attr(test, automock)]
pub trait PostService {
    fn index(&self, sr: &ServiceRepo, pk: i64) -> GeneralResult<Post>;

    fn search(
        &self,
        sr: &ServiceRepo,
        model: &SearchPost,
        pagination: &PaginationRequest,
    ) -> GeneralResult<PagedResponse<Post>>;

    fn delete(&self, sr: &ServiceRepo, pk: i64) -> GeneralResult<()>;

    fn update(&self, sr: &ServiceRepo, pk: i64, model: &UpdatePost) -> GeneralResult<()>;

    fn delete_with_token(&self, sr: &ServiceRepo, token: &str) -> GeneralResult<()>;

    fn is_with_parent(&self, sr: &ServiceRepo, post_id: i64) -> GeneralResult<bool>;

    fn create_with_upload(
        &self,
        sr: &ServiceRepo,
        form: NewPost,
        maybe_file: Option<File>,
    ) -> GeneralResult<Post>;
}

pub struct PostServiceImpl;

#[derive(Debug, Deserialize, Serialize, Queryable)]
pub struct Post {
    pub id: i64,
    pub title: Option<String>,
    pub content: Option<String>,
    pub category: Option<String>,

    #[serde(skip_serializing)]
    pub token_owner: String,

    pub parent_post_id: Option<i64>,
    pub doc_path: Option<String>,
    pub created_at: chrono::NaiveDateTime,
    pub updated_at: chrono::NaiveDateTime,
}

#[derive(Debug, Deserialize, Serialize, Insertable, Clone, Validate)]
#[table_name = "post"]
pub struct NewPost {
    pub title: Option<String>,

    #[validate(length(min = 5))]
    pub content: String,
    pub category: String,
    pub token_owner: String,
    pub doc_path: Option<String>,
    pub parent_post_id: Option<i64>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct SearchPost {
    pub title: Option<String>,
    pub category: Option<String>,
    pub content: Option<String>,
    pub token_owner: Option<String>,
    pub parent_post_id: Option<i64>,
    pub is_top_parent: Option<bool>,
}

#[derive(Debug, Deserialize, Serialize, AsChangeset)]
#[table_name = "post"]
pub struct UpdatePost {
    title: Option<String>,
    content: Option<String>,
    category: Option<String>,
}

#[derive(Debug, PartialEq, EnumString)]
pub enum PostCategory {
    Front,
    Meta,
}

impl PostService for PostServiceImpl {
    fn index(&self, sr: &ServiceRepo, pk: i64) -> GeneralResult<Post> {
        sr.r.post.index(sr, pk)
    }

    fn search(
        &self,
        sr: &ServiceRepo,
        model: &SearchPost,
        pagination: &PaginationRequest,
    ) -> GeneralResult<PagedResponse<Post>> {
        let (data, total, page) = sr.r.post.search(sr, model, pagination)?;
        Ok(PagedResponse::new(data, total, page))
    }

    fn delete(&self, sr: &ServiceRepo, pk: i64) -> GeneralResult<()> {
        // TODO: need to refactor all of this outside and make transaction to only happen in the repository
        // TODO: maybe use datalogic for connecting between repository?
        sr.config.conn().transaction(|| {
            let mut children_doc_path =
                sr.r.post
                    .get_child_doc_path_by_id(sr, pk)?
                    .into_iter()
                    .flatten()
                    .collect::<Vec<String>>();

            sr.s.post
                .index(sr, pk)
                .map(|p| p.doc_path.map(|path| children_doc_path.push(path)))?;

            // TODO: make filesystem a repository
            for path in children_doc_path {
                LocalFS::delete(path).map_err(|_| Error::RollbackTransaction)?;
            }

            sr.r.post.delete_by_id(sr, pk)
        })?;

        Ok(())
    }

    fn update(&self, sr: &ServiceRepo, pk: i64, model: &UpdatePost) -> GeneralResult<()> {
        if let Some(category) = &model.category {
            let _ = PostCategory::from_str(category).map_err(|_| {
                GeneralError::Form(format!(
                    "Value of \"{}\" for category is not recognizable",
                    category
                ))
            });
        }

        let _ = sr.r.post.update(sr, model, pk)?;
        Ok(())
    }

    fn delete_with_token(&self, sr: &ServiceRepo, token: &str) -> GeneralResult<()> {
        let _ = sr.r.post.delete_by_token(sr, token)?;
        Ok(())
    }

    fn is_with_parent(&self, sr: &ServiceRepo, post_id: i64) -> GeneralResult<bool> {
        let post = sr.r.post.index(sr, post_id)?;

        Ok(post.parent_post_id.is_some())
    }

    fn create_with_upload(
        &self,
        sr: &ServiceRepo,
        mut form: NewPost,
        maybe_file: Option<File>,
    ) -> GeneralResult<Post> {
        let _ = PostCategory::from_str(form.category.as_str()).map_err(|_| {
            GeneralError::Form(format!(
                "Value of \"{}\" for category is not recognizable",
                form.category
            ))
        });

        // TODO: need to refactor all of this outside and make transaction to only happen in the repository
        sr.config.conn().transaction(move || {
            // Check if parent doesn't have parent
            if let Some(parent_post_id) = form.parent_post_id {
                if sr.s.post.is_with_parent(sr, parent_post_id)? {
                    return Err(GeneralError::Form(
                        "The parent of your post has another parent, which is not allowed"
                            .to_string(),
                    ));
                }
            }

            // Populate the doc_path first
            // TODO: make LocalFS into filesystem repo
            if let Some(file) = &maybe_file {
                form.doc_path = Some(LocalFS::checked_random_filename(
                    file.sanitized_file_name(),
                )?);
            }

            let result = sr.r.post.create(sr, &form);

            // Save the file after the post is inserted so it would rollback if saving file is not successful
            // TODO: make LocalFS into filesystem repo
            if let (Some(file), Some(path), Ok(_)) = (maybe_file, form.doc_path, &result) {
                LocalFS::save(path, file)?;
            }

            result
        })
    }
}

// TODO: delete and change into the new way

// impl<'a> CommonModel<'a> for Post {
//     type PrimaryKey = i64;

//     type InsertableModel = NewPost;

//     type UpdateModel = UpdatePost;

//     type SearchModel = SearchPost;

//     type Table = post::table;

//     type Action = ();

//     fn index(conn: &DbConn, _: &JwtRequest, pk: Self::PrimaryKey) -> GeneralResult<Self> {
//         Ok(post::table.find(pk).first(conn)?)
//     }

//     fn delete(conn: &DbConn, jwt: &JwtRequest, pk: Self::PrimaryKey) -> GeneralResult<()> {
//         conn.transaction(|| {
//             let mut children_doc_path = post::table
//                 .select(post::doc_path)
//                 .filter(post::parent_post_id.eq(pk))
//                 .load::<Option<String>>(conn)?
//                 .into_iter()
//                 .flatten()
//                 .collect::<Vec<String>>();

//             Self::index(conn, jwt, pk)
//                 .map(|p| p.doc_path.map(|path| children_doc_path.push(path)))?;

//             for path in children_doc_path {
//                 LocalFS::delete(path).map_err(|_| Error::RollbackTransaction)?;
//             }

//             delete(post::table.find(pk)).execute(conn)
//         })?;

//         Ok(())
//     }

//     fn create(
//         _conn: &DbConn,
//         _: &JwtRequest,
//         _model: Self::InsertableModel,
//     ) -> GeneralResult<Self> {
//         unreachable!("You shouldn't use this method.")
//     }

//     fn update(
//         conn: &DbConn,
//         _: &JwtRequest,
//         model: Self::UpdateModel,
//         pk: Self::PrimaryKey,
//     ) -> GeneralResult<Self> {
//         Ok(conn.transaction(|| update(post::table.find(pk)).set(model).get_result(conn))?)
//     }

//     fn search(
//         conn: &DbConn,
//         _: &JwtRequest,
//         model: &Self::SearchModel,
//         pagination: &PaginationRequest,
//     ) -> GeneralResult<PagedResponse<Self>> {
//         // Retrieving Data
//         let mut select = post::table.into_boxed();

//         // Filtering
//         let used_model = model.clone();
//         if let Some(is_top) = used_model.is_top_parent {
//             match is_top {
//                 true => select = select.filter(post::parent_post_id.is_null()),
//                 false => select = select.filter(post::parent_post_id.is_not_null()),
//             };
//         }

//         if let Some(q) = used_model.title.clone() {
//             select = select.filter(post::title.ilike(q));
//         }

//         if let Some(q) = used_model.content.clone() {
//             select = select.filter(post::content.ilike(q));
//         }

//         if let Some(q) = used_model.parent_post_id {
//             select = select.filter(post::parent_post_id.eq(q))
//         }

//         // Retrieval and Pagination
//         let (data, total, page) = retrieve_and_paginate(conn, select, pagination)?;

//         Ok(PagedResponse::new(data, total, page))
//     }

//     fn authorize(
//         _user: &User,
//         _action: Self::Action,
//     ) -> GeneralResult<AuthExpression<Self::Table>> {
//         todo!()
//     }
// }

// impl Post {
//     pub fn delete_with_token(conn: &DbConn, jwt: &JwtRequest, token: &str) -> GeneralResult<()> {
//         let post_id: i64 = post::table
//             .select(post::id)
//             .filter(post::token_owner.eq(token))
//             .first(conn)?;

//         Post::delete(conn, jwt, post_id)?;

//         Ok(())
//     }

//     pub fn is_with_parent(conn: &DbConn, post_id: i64) -> GeneralResult<bool> {
//         let post = post::table
//             .filter(post::id.eq(post_id))
//             .get_result::<Post>(conn)?;

//         Ok(post.parent_post_id.is_some())
//     }

//     pub fn create_with_upload(
//         conn: &DbConn,
//         _jwt: &JwtRequest,
//         mut form: NewPost,
//         maybe_file: Option<File>,
//     ) -> GeneralResult<Self> {
//         Ok(conn.transaction(move || {
//             // Check if parent doesn't have parent
//             if let Some(parent_post_id) = form.parent_post_id {
//                 if Post::is_with_parent(conn, parent_post_id)? {
//                     return Err(GeneralError::Form(
//                         "The parent of your post has another parent, which is not allowed"
//                             .to_string(),
//                     )
//                     .into());
//                 }
//             }

//             // Populate the doc_path first
//             if let Some(file) = &maybe_file {
//                 form.doc_path = Some(LocalFS::checked_random_filename(
//                     file.sanitized_file_name(),
//                 )?);
//             }

//             let result = insert_into(post::table).values(&form).get_result(conn);

//             // Save the file after the post is inserted so it would rollback if saving file is not successful
//             if let (Some(file), Some(path), Ok(_)) = (maybe_file, form.doc_path, &result) {
//                 LocalFS::save(path, file)?;
//             }

//             result
//         })?)
//     }
// }

// #[cfg(test)]
// mod test {
//     use crate::db_prelude::test::create_test;
//     use crate::db_prelude::LocalFS;
//     use crate::middleware::fs::LOCAL_STORAGE_TEST_LOCATION;
//     use crate::service::post::{NewPost, Post};

//     #[test]
//     pub fn ok_create_new_post_without_media() {
//         // ARRANGE
//         let (pool, _, jwt) = create_test().expect("Cannot create new admin");
//         let conn = pool.get().unwrap();

//         let model = NewPost {
//             title: Some("Test Model".to_owned()),
//             content: "Test Content".to_owned(),
//             token_owner: "Test Token".to_owned(),
//             doc_path: None,
//             parent_post_id: None,
//         };

//         // ACT
//         let created = Post::create_with_upload(&conn, &jwt, model.clone(), None)
//             .expect("Failed creating post");

//         // ASSERT
//         assert_eq!(model.title, created.title);
//         assert_eq!(
//             model.content,
//             created.content.expect("Content should not be null")
//         );
//         assert_eq!(model.token_owner, created.token_owner);
//         assert_eq!(model.doc_path, created.doc_path);
//         assert_eq!(model.parent_post_id, created.parent_post_id);
//     }

//     #[test]
//     pub fn ok_create_new_post_with_media() {
//         // ARRANGE
//         let (pool, _, jwt) = create_test().expect("Cannot create new admin");
//         let conn = pool.get().unwrap();

//         let fake_file_size = 1_000_000;
//         let fake_file = LocalFS::create_fake_file("coba.txt".to_string(), fake_file_size);
//         let model = NewPost {
//             title: Some("Test Model".to_owned()),
//             content: "Test Content".to_owned(),
//             token_owner: "Test Token".to_owned(),
//             doc_path: None,
//             parent_post_id: None,
//         };

//         // ACT
//         let created = Post::create_with_upload(&conn, &jwt, model, Some(fake_file))
//             .expect("Failed creating post");

//         // ASSERT - FILE EXISTENCE
//         let saved_path = created.doc_path.expect("Doc path should exist");
//         let content =
//             std::fs::read_to_string(format!("{}/{}", LOCAL_STORAGE_TEST_LOCATION, saved_path))
//                 .expect("File doesn't exist or can't be read");
//         assert_eq!(content.len(), fake_file_size);
//     }
// }
