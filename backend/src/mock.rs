#[allow(dead_code)]
pub enum Realness<T> {
    Real(T),
    Mock,
}

impl<T> AsRef<T> for Realness<T> {
    fn as_ref(&self) -> &T {
        match self {
            Realness::Real(t) => t,
            Realness::Mock => {
                panic!("You're accessing a mocked attribute, consider using a real one.")
            }
        }
    }
}
