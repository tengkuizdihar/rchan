#[cfg(test)]
#[macro_use]
extern crate mockall;

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate num_derive;

pub(crate) mod handler;
pub(crate) mod mock;
pub(crate) mod prelude;
pub(crate) mod repository;
pub(crate) mod service;
pub(crate) mod util;

use crate::handler::{test, user};
use crate::util::request::rate_limiter;
use crate::util::server::*;
use crate::{handler::document, repository::psql::establish_connection};

use actix_cors::Cors;
use actix_web::web::Data;
use actix_web::{dev::Service, http::header, web::scope, App, HttpServer};
use awmp::PartsConfig;
use handler::post;
use log::{debug, info};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let app_config = ApplicationConfiguration::new();
    let outside_app_config = app_config.clone();
    let socket_address = outside_app_config.socket_address;

    // Establish the connection pool towards the database
    // We're currently using PostgreSQL right now
    let db_conn_pool = establish_connection();

    // Create a super admin if it doesn't exist yet
    let _ = establish_super_admin(db_conn_pool.clone());

    let mut server = HttpServer::new(move || {
        let app_config = app_config.clone();

        // CORS settings
        let cors = match app_config.preset {
            ApplicationPreset::Production => {
                Cors::default()
                    // God I wish
                    .allowed_origin("https://www.rchan.org")
                    .allowed_origin("http://localhost:5000")
                    .allowed_headers(vec![
                        header::AUTHORIZATION,
                        header::ACCEPT,
                        header::CONTENT_TYPE,
                    ])
            }
            ApplicationPreset::Debug => Cors::permissive(),
        };

        let parts = PartsConfig::default()
            .with_file_limit(5_000_000)
            .with_text_limit(5_000_000)
            .with_temp_dir("./storage");

        let mut app = App::new()
            // Need to upgrade cors and rate_limiter
            .wrap(cors)
            .wrap(rate_limiter())
            // Prints out requests method's and path to STDOUT
            .wrap_fn(|req, srv| {
                debug!("{} - {}", req.method(), req.path());
                srv.call(req)
            })
            // Use the already established connection pool
            .app_data(Data::new(db_conn_pool.clone()))
            .app_data(Data::new(parts))
            .app_data(Data::new(app_config.clone()));

        // Define the user services
        app = app.service(
            scope("/users")
                .service(user::detail)
                .service(user::whoami)
                .service(user::list)
                .service(user::create)
                .service(user::update)
                .service(user::delete)
                .service(user::login)
                .service(user::login_anon),
        );

        // Define the post services
        app = app.service(
            scope("/posts")
                .service(post::detail)
                .service(post::list)
                .service(post::create)
                .service(post::delete),
        );

        // Define the document services
        app = app.service(scope("/documents").service(document::index));

        // testing only scopes
        if let ApplicationPreset::Debug = &app_config.preset {
            app = app.service(
                scope("/test")
                    .service(test::index)
                    .service(test::document_upload_awmp)
                    .service(test::multiple_upload)
                    .service(test::nested_parameter),
            )
        };

        app
    });

    // Make the application into one thread when on debug mode
    if let ApplicationPreset::Debug = &outside_app_config.preset {
        server = server.workers(1);
    }

    // Bind the server to use a single socket address
    server = server.bind(&socket_address)?;

    // Welcome message when starting the application
    info!("Running on http://{}", &socket_address);

    // Run the server!
    server.run().await
}
