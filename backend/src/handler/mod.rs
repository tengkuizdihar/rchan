pub(crate) mod document;
pub(crate) mod post;
pub(crate) mod test;
pub(crate) mod user;

#[allow(dead_code)]
pub(crate) mod errors {
    use actix_web::{error::BlockingError, http::StatusCode, HttpResponse, ResponseError};
    use serde::{ser::SerializeStruct, Serialize};
    use std::fmt::Debug;
    use thiserror::Error;

    #[derive(Debug)]
    pub enum ErrorFault {
        Client,
        Server,
    }

    impl std::fmt::Display for ErrorFault {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self)
        }
    }

    #[derive(Error, Debug)]
    #[allow(dead_code)]
    pub enum GeneralError {
        #[error("Error with {status_code} => {message}")]
        Hardcoded { status_code: u16, message: String },

        #[error("Form Error: {0}")]
        Form(String),

        #[error("File Error made by {1} => {0}")]
        File(String, ErrorFault),

        #[error("IoError")]
        Io(String),

        #[error("IncompleteFormDataError")]
        IncompleteFormData,

        #[error("{0}")]
        Database(#[from] diesel::result::Error),

        #[error("Internal Database Connection Error")]
        InternalDBConnection(#[from] r2d2::Error),

        #[error("Query String Parse Error")]
        QueryStringParse(#[from] serde_qs::Error),

        #[error("Upload File Error")]
        UploadFile(#[from] awmp::Error),

        #[error("Backend Async Error")]
        BackendAsync,

        /// Used when the client can't be identified as any user in the system
        #[error("Authentication Error")]
        Authentication,

        /// Used when the user doesn't have any right to access a resource or action
        #[error("Authorization Error: {0}")]
        Authorization(String),

        #[error("Password hashing error")]
        PasswordHashing,

        #[error("Validation error: {0}")]
        Validation(#[from] validator::ValidationErrors),
    }

    impl Serialize for GeneralError {
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::Serializer,
        {
            let mut s = serializer.serialize_struct("GeneralError", 1)?;
            s.serialize_field("message", &format!("{}", self))?;
            s.end()
        }
    }

    impl ResponseError for GeneralError {
        fn status_code(&self) -> actix_web::http::StatusCode {
            let default = StatusCode::INTERNAL_SERVER_ERROR;

            match self {
                GeneralError::Hardcoded {
                    status_code,
                    message: _,
                } => StatusCode::from_u16(*status_code).unwrap_or(default),

                GeneralError::File(_, f) => match f {
                    ErrorFault::Client => StatusCode::BAD_REQUEST,
                    ErrorFault::Server => StatusCode::INTERNAL_SERVER_ERROR,
                },

                GeneralError::Form(_)
                | GeneralError::IncompleteFormData
                | GeneralError::Validation(_)
                | GeneralError::QueryStringParse(_) => StatusCode::UNPROCESSABLE_ENTITY,

                GeneralError::Database(diesel::result::Error::NotFound) => StatusCode::NOT_FOUND,

                GeneralError::Authorization(_) | GeneralError::Authentication => {
                    StatusCode::UNAUTHORIZED
                }

                GeneralError::Io(_)
                | GeneralError::InternalDBConnection(_)
                | GeneralError::UploadFile(_)
                | GeneralError::BackendAsync
                | GeneralError::PasswordHashing
                | GeneralError::Database(_) => default,
            }
        }

        fn error_response(&self) -> actix_web::HttpResponse {
            HttpResponse::build(self.status_code()).json(self)
        }
    }

    impl From<BlockingError> for GeneralError {
        fn from(_: BlockingError) -> Self {
            GeneralError::BackendAsync
        }
    }

    impl From<GeneralError> for diesel::result::Error {
        fn from(e: GeneralError) -> Self {
            match e {
                GeneralError::Database(e) => e,
                e => {
                    log::error!("Error most likely in transaction {}", e);
                    Self::RollbackTransaction
                }
            }
        }
    }
}

pub(crate) mod data {
    use actix_web::web::{Form, Json};
    use actix_web_validator::Validate;
    use serde::{Deserialize, Serialize};
    use validator::ValidationErrors;

    use super::errors::GeneralError;

    pub type GeneralResult<T> = Result<T, GeneralError>;
    pub type JsonResponse<T> = GeneralResult<Json<GeneralResponse<T>>>;

    #[derive(Serialize, Deserialize, Debug)]
    pub struct GeneralResponse<T> {
        message: String,
        payload: T,
    }

    #[derive(Serialize, Deserialize, Debug)]
    pub struct PaginationRequest {
        pub limit: Option<i64>,
        pub page: Option<i64>,
    }

    #[derive(Serialize, Deserialize, Debug)]
    pub struct PaginationResponse {
        pub total: i64,
        pub page: i64,
    }

    #[derive(Serialize, Deserialize, Debug)]
    pub struct PagedResponse<T> {
        pub data: Vec<T>,
        pub pagination: PaginationResponse,
    }

    pub trait ToValidatedForm<T>: Sized {
        fn validated_form(self) -> Result<T, validator::ValidationErrors>;
    }

    pub trait OptionToGeneralError<T> {
        fn to_general_error(self) -> GeneralResult<T>;
    }

    impl<T> PagedResponse<T> {
        pub fn new(data: Vec<T>, total: i64, page: i64) -> Self {
            PagedResponse {
                data,
                pagination: PaginationResponse { total, page },
            }
        }
    }

    impl<T> OptionToGeneralError<T> for Option<T>
    where
        T: num_traits::FromPrimitive,
    {
        fn to_general_error(self) -> GeneralResult<T> {
            self.ok_or(GeneralError::Hardcoded {
                status_code: 500,
                message: "Enum failed to parse".to_string(),
            })
        }
    }

    impl<T> ToValidatedForm<T> for Form<T>
    where
        T: Validate,
    {
        fn validated_form(self) -> Result<T, ValidationErrors> {
            self.validate()?;
            Ok(self.into_inner())
        }
    }

    impl<T> ToValidatedForm<T> for T
    where
        T: Validate,
    {
        fn validated_form(self) -> Result<T, ValidationErrors> {
            self.validate()?;
            Ok(self)
        }
    }

    impl PaginationRequest {
        pub fn get_pagination(&self, max_limit: i64) -> (i64, i64) {
            let limited = self.limit.map(|l| l.abs().max(max_limit)).unwrap_or(10i64);
            let page = self.page.unwrap_or_default().abs();

            (limited, page)
        }
    }

    impl<T> GeneralResponse<T> {
        pub fn new(message: String, payload: T) -> Json<Self> {
            Json(Self { message, payload })
        }
    }
}

pub(crate) mod auth {
    use crate::{handler::errors::GeneralError, ApplicationConfiguration};
    use actix_web::{http::header::AUTHORIZATION, web::Data, FromRequest};
    use jsonwebtoken::{decode, Algorithm, DecodingKey, Validation};
    use serde::{Deserialize, Serialize};
    use std::{
        future::{ready, Ready},
        ops::Deref,
    };

    #[derive(Debug, Serialize, Deserialize)]
    pub struct Claims {
        pub exp: i64, // Required (validate_exp defaults to true in validation). Expiration time (as UTC timestamp)
        pub iss: String, // Optional. Issuer
        pub sub: String, // Optional. Subject (whom token refers to)
    }

    #[derive(Debug)]
    pub struct JwtRequest(pub Claims);

    impl JwtRequest {
        /// Create a new JwtRequest based on the configuration given
        /// This function isn't supposed to be used outside of the scope of FromRequest
        pub fn new(
            token: &str,
            conf: &Data<ApplicationConfiguration>,
        ) -> Result<Self, GeneralError> {
            let validation = &Validation::new(Algorithm::HS256);
            let secret = DecodingKey::from_secret(conf.secret_key.as_bytes());

            match decode(token, &secret, validation) {
                Ok(j) => Ok(Self(j.claims)),
                Err(e) => {
                    log::error!("Error getting claims! {}", e);
                    Err(GeneralError::Authentication)
                }
            }
        }

        #[cfg(test)]
        pub fn new_test() -> Self {
            JwtRequest(Claims {
                exp: i64::MAX,
                iss: "testissuer".to_string(),
                sub: "testuser@test.com".to_string(),
            })
        }
    }

    impl FromRequest for JwtRequest {
        type Error = GeneralError;

        type Future = Ready<Result<Self, Self::Error>>;

        fn from_request(
            req: &actix_web::HttpRequest,
            _: &mut actix_web::dev::Payload,
        ) -> Self::Future {
            // Retrieve the configuration of the application
            // Supposedly only use the secret_key
            let conf = match req.app_data::<Data<ApplicationConfiguration>>() {
                Some(c) => c,
                None => {
                    return ready(Err(GeneralError::Hardcoded {
                        status_code: 500,
                        message: "Application configuration not found. HOW.".to_string(),
                    }))
                }
            };

            // Retreive the string token from authorization headers
            let tokenraw = req
                .headers()
                .get(AUTHORIZATION)
                .map(|h| {
                    h.to_str().map_err(|_| GeneralError::Hardcoded {
                        status_code: 400,
                        message: "Congratulation, you broke the parsing codes.".to_string(),
                    })
                })
                // Will split the string into two, discard the first, and collect the rest to string
                // String might be empty if the token is empty
                .map(|s| s.map(|op| op.split(' ').skip(1).collect::<String>()));

            // Create a JWTRequest with the claims in it
            if let Some(Ok(token)) = tokenraw {
                if !token.is_empty() {
                    ready(Self::new(token.as_ref(), conf))
                } else {
                    ready(Err(GeneralError::Authentication))
                }
            } else {
                ready(Err(GeneralError::Authentication))
            }
        }
    }

    impl Deref for JwtRequest {
        type Target = Claims;

        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }
}
