use super::data::PaginationRequest;
use actix_web::{
    delete, get, post, put,
    web::Data,
    web::{block, Form},
    web::{Path, Query},
};

use crate::{
    handler::{
        auth::JwtRequest,
        data::{GeneralResponse, JsonResponse, PagedResponse},
    },
    prelude::*,
    repository::psql::DbPool,
    service::{
        user::{
            LoginNative, NewUserRaw, SearchUser, UpdateUserRaw, User, UserService, UserServiceImpl,
        },
        ServiceRepo,
    },
    ApplicationConfiguration,
};

#[get("/{identification}")]
pub async fn detail(jwt: JwtRequest, db_pool: Data<DbPool>, path: Path<i64>) -> JsonResponse<User> {
    let extracted_path = path.into_inner();
    let conn = db_pool.get()?;

    let found_user = block(move || {
        let sr = ServiceRepo::new(conn);
        sr.s.user.index(&sr, &jwt, extracted_path)
    })
    .await??;

    Ok(GeneralResponse::new(
        "Succesfully getting user!".to_string(),
        found_user,
    ))
}

#[get("/who/ami")]
pub async fn whoami(jwt: JwtRequest, db_pool: Data<DbPool>) -> JsonResponse<User> {
    let conn = db_pool.get()?;
    let found_user = block(move || {
        let sr = ServiceRepo::new(conn);
        sr.s.user.whoami(&sr, &jwt)
    })
    .await??;

    Ok(GeneralResponse::new(
        "Succesfully getting user!".to_string(),
        found_user,
    ))
}

#[get("")]
pub async fn list(
    jwt: JwtRequest,
    db_pool: Data<DbPool>,
    search_form: Query<SearchUser>,
    pagination: Query<PaginationRequest>,
) -> JsonResponse<PagedResponse<User>> {
    let conn = db_pool.get()?;
    let found_user = block(move || {
        let sr = ServiceRepo::new(conn);
        sr.s.user.search(
            &sr,
            &jwt,
            &search_form.into_inner(),
            &pagination.into_inner(),
        )
    })
    .await??;

    Ok(GeneralResponse::new(
        "Sucessfully listing user!".to_string(),
        found_user,
    ))
}

#[post("")]
pub async fn create(
    jwt: JwtRequest,
    db_pool: Data<DbPool>,
    form: Form<NewUserRaw>,
) -> JsonResponse<User> {
    let conn = db_pool.get()?;
    let form = form.validated_form()?;

    let saved = block(move || {
        let sr = ServiceRepo::new(conn);
        sr.s.user.create(&sr, &jwt, form)
    })
    .await??;

    Ok(GeneralResponse::new(
        "Sucessfully create a new user!".to_string(),
        saved,
    ))
}

#[put("/{identification}")]
pub async fn update(
    jwt: JwtRequest,
    db_pool: Data<DbPool>,
    id: Path<i64>,
    form: Form<UpdateUserRaw>,
) -> JsonResponse<User> {
    let conn = db_pool.get()?;
    let form = form.validated_form()?;
    let updated = block(move || {
        let sr = ServiceRepo::new(conn);
        sr.s.user.update(&sr, &jwt, form, id.into_inner())
    })
    .await??;

    Ok(GeneralResponse::new(
        "Sucessfully create a new user!".to_string(),
        updated,
    ))
}

#[delete("/{identification}")]
pub async fn delete(jwt: JwtRequest, db_pool: Data<DbPool>, path: Path<i64>) -> JsonResponse<()> {
    let conn = db_pool.get()?;
    let identify = path.into_inner();

    block(move || {
        let sr = ServiceRepo::new(conn);
        sr.s.user.delete(&sr, &jwt, identify)
    })
    .await??;

    Ok(GeneralResponse::new(
        "Sucessfully deleting the user!".to_string(),
        (),
    ))
}

// NON-CRUD

#[post("/login")]
pub async fn login(
    db_pool: Data<DbPool>,
    conf: Data<ApplicationConfiguration>,
    form: Form<LoginNative>,
) -> JsonResponse<String> {
    let conn = db_pool.get()?;
    let jwt = block(move || {
        let sr = ServiceRepo::new(conn);
        sr.s.user
            .login(&sr, form.into_inner(), conf.secret_key.as_ref())
    })
    .await??;

    Ok(GeneralResponse::new(
        "Successfully logged in!".to_string(),
        jwt,
    ))
}

#[post("/login/anon")]
pub async fn login_anon(conf: Data<ApplicationConfiguration>) -> JsonResponse<String> {
    let jwt = UserServiceImpl.login_anon(conf.secret_key.as_ref())?;

    Ok(GeneralResponse::new(
        "Successfully logged in as anon!".to_string(),
        jwt,
    ))
}
