use super::data::{GeneralResponse, JsonResponse};
use crate::util::{fs::LocalFS, request::NestedQuery};
use actix_web::{get, post, web::Path, Responder};
use log::debug;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub struct QueryTest {
    reg_int: Option<i64>,
    reg_string: Option<String>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct FlatParam {
    pub text: Option<String>,
    pub number: Option<i32>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct NestedParam {
    pub text: Option<String>,
    pub nested: Option<FlatParam>,
}

#[get("/echo/{echo}")]
pub async fn index(path: Path<String>) -> JsonResponse<String> {
    let extracted_path = path.into_inner();
    Ok(GeneralResponse::new(
        "Successfully testing request!".to_string(),
        extracted_path,
    ))
}

#[post("/document-awmp")]
pub async fn document_upload_awmp(mut parts: awmp::Parts) -> JsonResponse<QueryTest> {
    let query_string = parts.texts.to_query_string();
    debug!("Query String Value: {}", query_string);

    let parsed: QueryTest = serde_qs::from_str(query_string.as_str())?;
    let file = parts.files.take("file").pop();

    match file {
        Some(f) => {
            let file_name = f.sanitized_file_name();
            debug!("Sanitized filename: {}", file_name);

            let new_file_path = LocalFS::checked_random_filename(file_name)?;
            let result = f.persist_at(new_file_path);

            debug!("File after persist status: {:?}", result);

            Ok(GeneralResponse::new(
                "Upload document successful.".to_string(),
                parsed,
            ))
        }
        None => Ok(GeneralResponse::new(
            "Upload document unsuccessful.".to_string(),
            parsed,
        )),
    }
}

#[get("/nested-parameter")]
pub async fn nested_parameter(param: NestedQuery<NestedParam>) -> impl Responder {
    actix_web::HttpResponse::Ok().json(param.into_inner())
}

#[post("/multiple-upload")]
pub async fn multiple_upload(mut parts: awmp::Parts) -> JsonResponse<Vec<String>> {
    let chosen_files = parts.files.take("file");
    let mut file_names = Vec::with_capacity(chosen_files.len());

    for file in chosen_files {
        let file_name = LocalFS::checked_random_filename(file.sanitized_file_name())?;

        file_names.push(file_name.clone());

        LocalFS::save(file_name, file)?;
    }

    Ok(GeneralResponse::new(
        "Successfully upload multiple files".to_string(),
        file_names,
    ))
}
