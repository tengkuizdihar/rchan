use crate::{
    handler::data::{GeneralResponse, JsonResponse, PagedResponse},
    prelude::*,
    repository::psql::DbPool,
    service::{post::*, ServiceRepo},
};

use actix_web::{
    delete, get, post,
    web::{block, Data, Path, Query},
};

use super::data::PaginationRequest;

#[get("/{identification}")]
pub async fn detail(db_pool: Data<DbPool>, path: Path<i64>) -> JsonResponse<Post> {
    let pk = path.into_inner();
    let conn = db_pool.get()?;

    let found_post = block(move || {
        let sr = ServiceRepo::new(conn);
        sr.s.post.index(&sr, pk)
    })
    .await??;

    Ok(GeneralResponse::new(
        "Successfully getting post!".to_string(),
        found_post,
    ))
}

#[get("")]
pub async fn list(
    db_pool: Data<DbPool>,
    search_form: Query<SearchPost>,
    pagination: Query<PaginationRequest>,
) -> JsonResponse<PagedResponse<Post>> {
    let conn = db_pool.get()?;
    let found_post = block(move || {
        let sr = ServiceRepo::new(conn);
        sr.s.post
            .search(&sr, &search_form.into_inner(), &pagination.into_inner())
    })
    .await??;

    Ok(GeneralResponse::new(
        "Successfully listing post!".to_string(),
        found_post,
    ))
}

#[post("")]
pub async fn create(db_pool: Data<DbPool>, mut parts: awmp::Parts) -> JsonResponse<Post> {
    // Get the file from the form-data and throw error if not found
    let maybe_file = parts.files.take("file").pop();
    let conn = db_pool.get()?;

    // Parse the form from the text part of the data form
    let form = serde_qs::from_str::<NewPost>(parts.texts.to_query_string().as_str())?;
    let form = form.validated_form()?;

    let post_created = block(move || {
        let sr = ServiceRepo::new(conn);
        sr.s.post.create_with_upload(&sr, form, maybe_file)
    })
    .await??;

    Ok(GeneralResponse::new(
        "Successfully create a new post!".to_string(),
        post_created,
    ))
}

#[delete("/{token}")]
pub async fn delete(db_pool: Data<DbPool>, path: Path<String>) -> JsonResponse<()> {
    let conn = db_pool.get()?;
    let token = path.into_inner();

    block(move || {
        let sr = ServiceRepo::new(conn);
        sr.s.post.delete_with_token(&sr, token.as_str())
    })
    .await??;

    Ok(GeneralResponse::new(
        "Successfully deleting the post!".to_string(),
        (),
    ))
}
