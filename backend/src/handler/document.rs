use crate::handler::{data::GeneralResult, errors::GeneralError};

use actix_web::{get, web::Path};

#[get("/{filepath}")]
pub async fn index(path: Path<String>) -> GeneralResult<actix_files::NamedFile> {
    let doc_path = format!("./storage/{}", path.into_inner());

    match actix_files::NamedFile::open(doc_path) {
        Ok(f) => Ok(f),
        Err(_) => Err(GeneralError::Io("File failed to be opened".to_string())),
    }
}
