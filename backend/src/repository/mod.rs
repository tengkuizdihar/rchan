use crate::repository::psql::{
    post::{PostRepository, PostRepositoryImpl},
    user::{UserRepository, UserRepositoryImpl},
};

pub mod psql;

pub struct Repository {
    pub user: Box<dyn UserRepository>,
    pub post: Box<dyn PostRepository>,
}

impl Repository {
    pub fn new() -> Self {
        Repository {
            user: Box::new(UserRepositoryImpl),
            post: Box::new(PostRepositoryImpl),
        }
    }

    #[cfg(test)]
    pub fn mock() -> Repository {
        use crate::repository::psql::{post::MockPostRepository, user::MockUserRepository};

        Repository {
            user: Box::new(MockUserRepository::new()),
            post: Box::new(MockPostRepository::new()),
        }
    }
}
