use std::env;

use diesel::pg::Pg;
use diesel::prelude::*;
use diesel::query_builder::*;
use diesel::query_dsl::methods::LoadQuery;
use diesel::r2d2::ConnectionManager;
use diesel::sql_types::BigInt;
use diesel::sql_types::Bool;
use diesel::types::HasSqlType;

use r2d2::{Pool, PooledConnection};

use crate::handler::data::PaginationRequest;

embed_migrations!();

pub type DbPool = Pool<ConnectionManager<PgConnection>>;
pub type DbConn = PooledConnection<ConnectionManager<PgConnection>>;
pub type AuthExpression<T> = Box<dyn BoxableExpression<T, Pg, SqlType = Bool>>;
pub const DEFAULT_PER_PAGE: i64 = 10;

pub mod post;
pub mod schema;
pub mod user;

pub trait Paginate: Sized {
    fn paginate(self, page: i64) -> Paginated<Self>;
}

pub fn establish_connection() -> DbPool {
    dotenv::dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let manager = ConnectionManager::<PgConnection>::new(database_url);

    r2d2::Pool::builder()
        .max_size(10)
        .build(manager)
        .expect("Failed to create DB pool.")
}

/// A convenient function to retrieve and paginate a select query
/// It returns a triple tuple with the format of:
///     (vector of data, total pages, current page index)
///
/// For example:
/// ```
/// let conn: &DbConn = ...;
/// let pagination = PaginationRequest {
///     limit: Some(10),
///     page: Some(2)
/// };
/// let select = app_user::table.filter(app_user::id.is_not_null());
/// let (data, total, page) = retrieve_and_paginate(conn, select, pagination)?;
/// ```
pub fn retrieve_and_paginate<T, S>(
    conn: &PgConnection,
    select: S,
    pagination: &PaginationRequest,
) -> QueryResult<(Vec<T>, i64, i64)>
where
    S: QueryId + Query + QueryFragment<Pg>,
    T: Queryable<S::SqlType, Pg>,
    Pg: HasSqlType<S::SqlType>,
{
    let (limit, page) = pagination.get_pagination(10);
    let result = select
        .paginate(page)
        .per_page(std::cmp::min(limit, DEFAULT_PER_PAGE))
        .load_and_count_pages(conn)?;

    Ok((result.0, result.1, page))
}

impl<T> Paginate for T {
    fn paginate(self, page: i64) -> Paginated<Self> {
        Paginated {
            query: self,
            per_page: DEFAULT_PER_PAGE,
            page,
        }
    }
}

/// A pagination struct used to paginate a select query for the user.
/// To use pagination in your application, you may want to use [`crate::database::db::retrieve_and_paginate`]
/// instead.
#[derive(Debug, Clone, Copy, QueryId)]
pub struct Paginated<T> {
    query: T,
    page: i64,
    per_page: i64,
}

impl<T> Paginated<T> {
    pub fn per_page(self, per_page: i64) -> Self {
        Paginated { per_page, ..self }
    }

    pub fn load_and_count_pages<U>(self, conn: &PgConnection) -> QueryResult<(Vec<U>, i64)>
    where
        Self: LoadQuery<PgConnection, (U, i64)>,
    {
        let per_page = self.per_page;
        let results = self.load::<(U, i64)>(conn)?;
        let total = results.get(0).map(|x| x.1).unwrap_or(0);
        let records = results.into_iter().map(|x| x.0).collect();
        let total_pages = (total as f64 / per_page as f64).ceil() as i64;
        Ok((records, total_pages))
    }
}

impl<T: Query> Query for Paginated<T> {
    type SqlType = (T::SqlType, BigInt);
}

impl<T> RunQueryDsl<PgConnection> for Paginated<T> {}

impl<T> QueryFragment<Pg> for Paginated<T>
where
    T: QueryFragment<Pg>,
{
    fn walk_ast(&self, mut out: AstPass<Pg>) -> QueryResult<()> {
        out.push_sql("SELECT *, COUNT(*) OVER () FROM (");
        self.query.walk_ast(out.reborrow())?;
        out.push_sql(") t LIMIT ");
        out.push_bind_param::<BigInt, _>(&self.per_page)?;
        out.push_sql(" OFFSET ");
        let offset = (self.page - 1) * self.per_page;
        out.push_bind_param::<BigInt, _>(&offset)?;
        Ok(())
    }
}

pub mod prelude {
    pub use diesel::dsl::*;
    pub use diesel::expression_methods::*;
    pub use diesel::prelude::*;
    pub use diesel::Connection;
    pub use diesel::{delete, insert_into, update, QueryDsl, QueryResult, RunQueryDsl};
}

#[cfg(test)]
pub mod test {
    use std::env;
    use std::io::sink;

    use diesel::insert_into;
    use diesel::prelude::*;
    use diesel::r2d2::ConnectionManager;

    use diesel_migrations::MigrationConnection;
    use diesel_migrations::MigrationError;
    use pbkdf2::password_hash::SaltString;
    use rand_core::OsRng;

    use crate::handler::auth::JwtRequest;
    use crate::handler::data::GeneralResult;
    use crate::handler::errors::GeneralError;
    use crate::repository::psql::test;
    use crate::repository::psql::DbPool;
    use crate::service::user::salted_hash;
    use crate::service::user::NewUser;
    use crate::service::user::NewUserRaw;
    use crate::service::user::Role;
    use crate::service::user::User;
    use crate::util::fs::LocalFS;

    /// This will silently revert the latest migration.
    /// Silently as in, will not print the result to the STDOUT.
    /// Usually used in testing environment.
    pub fn revert_latest_migration_silent<C>(
        conn: &C,
    ) -> core::result::Result<(), diesel_migrations::RunMigrationsError>
    where
        C: MigrationConnection,
    {
        let migrations_dir = diesel_migrations::find_migrations_directory()?;

        diesel_migrations::setup_database(conn)?;

        let latest_migration_version = conn.latest_run_migration_version()?.ok_or(
            diesel_migrations::RunMigrationsError::MigrationError(MigrationError::NoMigrationRun),
        )?;

        diesel_migrations::revert_migration_with_version(
            conn,
            &migrations_dir,
            &latest_migration_version,
            &mut sink(),
        )
    }

    /// This will silently run every pending migrations.
    /// Silently as in, will not print the result to the STDOUT.
    /// Usually used in testing environment.
    pub fn run_pending_migrations_silent<C>(conn: &C)
    where
        C: MigrationConnection,
    {
        let migrations_dir =
            diesel_migrations::find_migrations_directory().expect("Migration directory not found.");

        diesel_migrations::run_pending_migrations_in_directory(conn, &migrations_dir, &mut sink())
            .expect("Running migrations for test failed");
    }

    /// This will silently reset the database.
    /// Silently as in, will not print the result to the STDOUT.
    pub fn reset_database(conn: &PgConnection) {
        // Will revert all migration available
        while revert_latest_migration_silent(conn).is_ok() {}

        // Run every single migration for testing
        let _ = run_pending_migrations_silent(conn);
    }

    pub fn establish_test_connection() -> super::DbPool {
        dotenv::dotenv().ok();
        let manager = ConnectionManager::<PgConnection>::new(
            env::var("TEST_DATABASE_URL").expect("TEST_DATABASE_URL must be set"),
        );
        let pool = r2d2::Pool::builder()
            .build(manager)
            .expect("Failed to create DB pool.");

        reset_database(&pool.get().unwrap());

        pool
    }

    pub fn create_test() -> GeneralResult<(DbPool, User, JwtRequest)> {
        LocalFS::refresh_local_test_storage();

        let pool = test::establish_test_connection();
        let form = NewUserRaw {
            email: "testuser@test.com".to_string(),
            display_name: "testuser".to_string(),
            role_id: Role::Admin as i32,
            password: "123456".to_string(),
        };

        let salt = SaltString::generate(&mut OsRng);

        match salted_hash(form.password.as_str(), &salt) {
            Ok(p) => {
                let new_model = NewUser {
                    email: form.email,
                    display_name: form.display_name,
                    role_id: form.role_id,
                    pass_hash: p.to_string(),
                };

                let conn = pool.get().unwrap();

                let test_user = conn.transaction(|| {
                    insert_into(crate::repository::psql::schema::app_user::table)
                        .values(new_model)
                        .get_result(&conn)
                })?;

                Ok((pool, test_user, JwtRequest::new_test()))
            }

            Err(e) => {
                log::error!("Password hashing error! {:?}", e);
                Err(GeneralError::Authentication)
            }
        }
    }
}
