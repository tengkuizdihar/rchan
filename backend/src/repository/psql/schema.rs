table! {
    app_user (id) {
        id -> Int8,
        email -> Varchar,
        display_name -> Varchar,
        role_id -> Int4,
        pass_hash -> Varchar,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    post (id) {
        id -> Int8,
        title -> Nullable<Text>,
        content -> Nullable<Text>,
        category -> Nullable<Varchar>,
        token_owner -> Varchar,
        parent_post_id -> Nullable<Int8>,
        doc_path -> Nullable<Varchar>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

allow_tables_to_appear_in_same_query!(
    app_user,
    post,
);
