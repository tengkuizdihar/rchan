use crate::{
    handler::{
        data::{GeneralResult, PaginationRequest},
        errors::GeneralError,
    },
    prelude::*,
    repository::psql::prelude::*,
    repository::psql::{retrieve_and_paginate, schema::app_user, AuthExpression},
    service::{
        user::{NewUser, Role, SearchUser, UpdateUser, User, UserAction},
        ServiceRepo,
    },
};

#[cfg_attr(test, automock)]
pub trait UserRepository {
    fn get_by_email(&self, sr: &ServiceRepo, email: &str) -> GeneralResult<User>;

    fn get_by_id_auth(&self, sr: &ServiceRepo, user: &User, pk: i64) -> GeneralResult<User>;

    fn search_auth(
        &self,
        sr: &ServiceRepo,
        actor: &User,
        model: &SearchUser,
        pagination: &PaginationRequest,
    ) -> GeneralResult<(Vec<User>, i64, i64)>;

    fn authorize_search<'a>(
        &self,
        sr: &ServiceRepo,
        user: &User,
        action: UserAction<'a>,
    ) -> GeneralResult<AuthExpression<app_user::table>>;

    fn delete_by_id_auth(&self, sr: &ServiceRepo, actor: &User, pk: i64) -> GeneralResult<()>;

    fn create(&self, sr: &ServiceRepo, model: NewUser) -> GeneralResult<User>;

    fn update_user_by_id_auth(
        &self,
        sr: &ServiceRepo,
        actor: &User,
        pk: i64,
        model: &UpdateUser,
    ) -> GeneralResult<User>;
}

pub struct UserRepositoryImpl;

impl UserRepository for UserRepositoryImpl {
    fn get_by_email(&self, sr: &ServiceRepo, email: &str) -> GeneralResult<User> {
        Ok(app_user::table
            .filter(app_user::email.eq(email))
            .first(sr.config.conn())?)
    }

    fn get_by_id_auth(&self, sr: &ServiceRepo, user: &User, pk: i64) -> GeneralResult<User> {
        Ok(app_user::table
            .filter(sr.r.user.authorize_search(sr, user, UserAction::Read)?)
            .filter(app_user::id.eq(pk))
            .first(sr.config.conn())?)
    }

    fn authorize_search(
        &self,
        _: &ServiceRepo,
        user: &User,
        action: UserAction,
    ) -> GeneralResult<AuthExpression<app_user::table>> {
        let role: Role = FromPrimitive::from_i32(user.role_id).to_general_error()?;

        match action {
            // Only admin can create user
            UserAction::Create => match role {
                Role::Admin => Ok(Box::new(app_user::id.is_not_null())),
                _ => Err(GeneralError::Authorization(
                    "Only admin can create user.".to_string(),
                )),
            },

            // Only admin can read all of the people's data
            UserAction::Read | UserAction::Delete => match role {
                Role::Admin => Ok(Box::new(app_user::id.is_not_null())),
                Role::Anonymous => Err(GeneralError::Authorization(
                    "Please log in to continue".to_string(),
                )),
                _ => Ok(Box::new(app_user::id.eq(user.id))),
            },

            // Only admin can update the roles of app_user
            UserAction::Update(u) => match u.role_id {
                Some(_) => match role {
                    Role::Admin => Ok(Box::new(app_user::id.is_not_null())),
                    _ => Err(GeneralError::Authorization(
                        "Only admin are able to change roles.".to_string(),
                    )),
                },
                None => match role {
                    Role::Admin => Ok(Box::new(app_user::id.is_not_null())),
                    Role::Anonymous => Err(GeneralError::Authorization(
                        "Please log in to continue".to_string(),
                    )),
                    _ => Ok(Box::new(app_user::id.eq(user.id))),
                },
            },
        }
    }

    fn delete_by_id_auth(&self, sr: &ServiceRepo, user: &User, id: i64) -> GeneralResult<()> {
        let auth_filter = sr.r.user.authorize_search(sr, user, UserAction::Delete)?;
        let _ = sr.config.conn().transaction(|| {
            diesel::delete(app_user::table.filter(app_user::id.eq(id).and(auth_filter)))
                .execute(sr.config.conn())
        })?;

        Ok(())
    }

    fn create(&self, sr: &ServiceRepo, model: NewUser) -> GeneralResult<User> {
        let user = sr.config.conn().transaction(|| {
            insert_into(app_user::table)
                .values(model)
                .get_result(sr.config.conn())
        })?;

        Ok(user)
    }

    fn update_user_by_id_auth(
        &self,
        sr: &ServiceRepo,
        actor: &User,
        pk: i64,
        model: &UpdateUser,
    ) -> GeneralResult<User> {
        let auth_filter =
            sr.r.user
                .authorize_search(sr, actor, UserAction::Update(model))?;
        let updated_user = sr.config.conn().transaction(|| {
            diesel::update(
                app_user::table
                    .filter(app_user::id.eq(pk))
                    .filter(auth_filter),
            )
            .set(model)
            .get_result(sr.config.conn())
        })?;

        Ok(updated_user)
    }

    fn search_auth(
        &self,
        sr: &ServiceRepo,
        actor: &User,
        model: &SearchUser,
        pagination: &PaginationRequest,
    ) -> GeneralResult<(Vec<User>, i64, i64)> {
        let mut select = app_user::table.into_boxed();
        select = select.filter(sr.r.user.authorize_search(sr, actor, UserAction::Read)?);

        // Filtering
        if let Some(q) = &model.id {
            select = select.filter(app_user::id.eq(q));
        }
        if let Some(q) = &model.email {
            select = select.filter(app_user::email.eq(q));
        }
        if let Some(q) = &model.display_name {
            select = select.filter(app_user::display_name.ilike(q));
        }

        let result = retrieve_and_paginate(sr.config.conn(), select, pagination)?;
        Ok(result)
    }
}
