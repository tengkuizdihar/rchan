use crate::{
    handler::data::{GeneralResult, PaginationRequest},
    repository::psql::prelude::*,
    repository::psql::{retrieve_and_paginate, schema::post},
    service::{
        post::{NewPost, Post, SearchPost, UpdatePost},
        ServiceRepo,
    },
};

#[cfg_attr(test, automock)]
pub trait PostRepository {
    fn index(&self, sr: &ServiceRepo, pk: i64) -> GeneralResult<Post>;

    fn search(
        &self,
        sr: &ServiceRepo,
        model: &SearchPost,
        pagination: &PaginationRequest,
    ) -> GeneralResult<(Vec<Post>, i64, i64)>;

    fn get_child_doc_path_by_id(
        &self,
        sr: &ServiceRepo,
        pk: i64,
    ) -> GeneralResult<Vec<Option<String>>>;

    fn get_parent_id_by_token(&self, sr: &ServiceRepo, token: &str) -> GeneralResult<i64>;

    fn create(&self, sr: &ServiceRepo, model: &NewPost) -> GeneralResult<Post>;

    fn delete_by_id(&self, sr: &ServiceRepo, pk: i64) -> GeneralResult<()>;

    fn delete_by_token(&self, sr: &ServiceRepo, token: &str) -> GeneralResult<()>;

    fn update(&self, sr: &ServiceRepo, model: &UpdatePost, pk: i64) -> GeneralResult<Post>;
}

pub struct PostRepositoryImpl;

impl PostRepository for PostRepositoryImpl {
    fn index(&self, sr: &ServiceRepo, pk: i64) -> GeneralResult<Post> {
        Ok(post::table.find(pk).first(sr.config.conn())?)
    }

    fn search(
        &self,
        sr: &ServiceRepo,
        model: &SearchPost,
        pagination: &PaginationRequest,
    ) -> GeneralResult<(Vec<Post>, i64, i64)> {
        // Retrieving Data
        let mut select = post::table.into_boxed();

        // Filtering
        let used_model = model.clone();
        if let Some(is_top) = used_model.is_top_parent {
            match is_top {
                true => select = select.filter(post::parent_post_id.is_null()),
                false => select = select.filter(post::parent_post_id.is_not_null()),
            };
        }

        if let Some(q) = used_model.title.clone() {
            select = select.filter(post::title.ilike(q));
        }

        if let Some(q) = used_model.content.clone() {
            select = select.filter(post::content.ilike(q));
        }

        if let Some(q) = used_model.parent_post_id {
            select = select.filter(post::parent_post_id.eq(q))
        }

        if let Some(q) = used_model.category {
            select = select.filter(post::category.eq(q))
        }

        // Retrieval and Pagination
        let result = retrieve_and_paginate(sr.config.conn(), select, pagination)?;
        Ok(result)
    }

    fn get_child_doc_path_by_id(
        &self,
        sr: &ServiceRepo,
        pk: i64,
    ) -> GeneralResult<Vec<Option<String>>> {
        Ok(post::table
            .select(post::doc_path)
            .filter(post::parent_post_id.eq(pk))
            .load::<Option<String>>(sr.config.conn())?)
    }

    fn get_parent_id_by_token(&self, sr: &ServiceRepo, token: &str) -> GeneralResult<i64> {
        Ok(post::table
            .select(post::id)
            .filter(post::token_owner.eq(token))
            .first(sr.config.conn())?)
    }

    fn create(&self, sr: &ServiceRepo, model: &NewPost) -> GeneralResult<Post> {
        Ok(insert_into(post::table)
            .values(model)
            .get_result(sr.config.conn())?)
    }

    fn delete_by_id(&self, sr: &ServiceRepo, pk: i64) -> GeneralResult<()> {
        let _ = delete(post::table.find(pk)).execute(sr.config.conn())?;
        Ok(())
    }

    fn delete_by_token(&self, sr: &ServiceRepo, token: &str) -> GeneralResult<()> {
        let _ =
            delete(post::table.filter(post::token_owner.eq(token))).execute(sr.config.conn())?;
        Ok(())
    }

    fn update(&self, sr: &ServiceRepo, model: &UpdatePost, pk: i64) -> GeneralResult<Post> {
        Ok(sr.config.conn().transaction(|| {
            update(post::table.find(pk))
                .set(model)
                .get_result(sr.config.conn())
        })?)
    }
}
