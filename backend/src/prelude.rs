//! This module should only be used for traits

pub use strum::EnumString;

pub use num_traits::{FromPrimitive, ToPrimitive};
pub use serde::{Deserialize, Serialize};
pub use validator::Validate;

pub use crate::handler::data::OptionToGeneralError;
pub use crate::handler::data::ToValidatedForm;
