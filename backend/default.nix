let
  d = import ./dependencies.nix { };
  pkgs = d.pkgs;
in
pkgs.rustPlatform.buildRustPackage rec {
  pname = "rchan";
  version = "0.0.1";

  src = ./.;

  buildInputs = d.buildInputs;

  cargoSha256 = "0qj6hxi9w0kminbaqsn1z6m1dns0ri2amq9cbfcmlx5z1h5z0dcn";

  # Disable because PgSQL service is needed to test properly
  doCheck = false;

  meta = with pkgs.lib; {
    description = "Some imageboard";
    homepage = "https://gitlab.com/tengkuizdihar/rchan";
    license = licenses.agpl3Only;
    maintainers = [ maintainers.tengkuizdihar ];
  };
}
