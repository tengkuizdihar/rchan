import http from "k6/http";
import { check, sleep } from "k6";

// ENVIRONMENT VARIABLE
const domain = "http://localhost:8000";

// TEST CONFIGURATION
export let options = {
  stages: [
    { duration: "2s", target: 1 },
    { duration: "5s", target: 100 },
    { duration: "5s", target: 1000 },
  ],
};

const global = {
  token:
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NjM3NjQ4MzUsImlzcyI6InJjaGFuIiwic3ViIjoiYWRtaW5AYWRtaW4uY29tIn0.EB3adm-0h5671TZCiBiQKVoGRZBiBILIbrO0L7fnaS0",
};

const postLists = () => {
  // Test the posts API and randomly changing the pages
  let pageIndex = Math.floor(Math.random() * 5 + 1);
  
  let getParam = `limit=10&page=${pageIndex}`;
  
  let res = http.get(`${domain}/posts?${getParam}`, {
    headers: {
      authorization: `Bearer ${global.token}`,
    }
  });

  check(res, { "status is 200 which is good": (r) => r.status === 200 });
};

const documentGet = () => {
  let res = http.get(`${domain}/documents/QwReRlHu4hZSOP9dOq16QF0L4hGkk3yB-Screenshot_2021-09-10_11-25-19.png`, {
    headers: {
      authorization: `Bearer ${global.token}`,
    }
  });

  check(res, { "status is 200 which is good": (r) => r.status === 200 });
}

// Mix and match functions in the main
export default function main() {
  postLists();
  documentGet();
}
