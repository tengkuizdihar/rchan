# Load Testing

To test the performance and quality of this software, I've made a workflow to test the website.
This guide would assume that you're already in a suitable environment based on `default.nix` on the project root.
As for the software itself, we're going to use [k6](https://k6.io/docs/).

# HOWTO - Run The Test

One of the most basic `k6` command is `k6 run <path to js script file>`.
Right now you could run the test by going inside of this folder and running `k6 run script.js` while the browser is open at `localhost:8000`. This command will run the default exported function in the script **once** and then quit itself when done.
When finished, there will be a diagnostic of the activity printed in your terminal.

To add more "virtual user" or basically parallel while loops and duration, you can use `k6 run --vus 10 --duration 30s script.js`.
The `--vus` flag stands for "virtual users" or the number of parallel jobs when running the script. In this case we're using 10 "virtual users".
The `--duration` itself is self explanatory.

> **NOTE**
> 
> For further explanation on how to modify your OS to suit a testing regime, you might want to read this documentation https://k6.io/docs/misc/fine-tuning-os/.

When you're testing the load of a program, you might be interested in running a lot more connection than your program might typically handle.
If there's a warning such as `dial tcp 127.0.0.1:8000: socket: too many open files"`, it means that the network resource limit has been reached, which will prevent k6 from creating new connections, thus altering the test result. In some cases this may be desired, to measure overall system performance, for example, but in most cases this will be a bottleneck towards testing the HTTP server and web application itself.

To increase the limit of a user, you could use `ulimit -Sa` for displaying the soft limit and `ulimit -Ha` to see the hard limit.
The user may change the limit of opened files by using `ulimit -n 5000`.
This command will not persist and will be reverted once you've exited your shell session.